+++
title = "Pointers"
weight = 1
draft = false
+++

## Pointers
- no pointer
```java
int num1 = 11;
int num2 = num1;

num1=22;

System.out.println("num1: " + num1); //num1: 22
System.out.println("num2: " + num2); //num2: 11
```

- pointer
```java
HashMap<String, Integer> map1 = new HashMap<>();
HashMap<String, Integer> map2 = new HashMap<>();

map1.put("value",11);
map2 = map1 //map1 and map2 are pointing the same hashmap in memory

map1.put("value",22);
System.out.println(map1);//{value=22}
System.out.println(map2);//{value=22}
```