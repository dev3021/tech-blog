+++
title = "IAM"
weight = 1
draft = false
+++

#### Manage IAM users and their access
- You can create users in IAM, assign them individual security credentials(In other words, access keys, passwords, and multi-factor authentication devices), or request temporary security credentials to provide users access to AWS services and resources.

<br>

#### Manage IAM roles and their permissions
- You can create roles in IAM and manage permissions to control which operations can be performed by the entity, or AWS service, that assumes the role.

<br>

#### Power User Creation
- To create an IAM User with admin-like privileges and programmatic acces. **PowerUserAccess** is similar to **AdministratorAccess** in that will allo users with this policy to do virtually anything in their AWS account except for managing IAM roles.

1. Navigate to IAM

2. Create **User Groups**
    - navigate to User Groups
    - create a user group: "power-users"
    - attach permissions policies
      - you can sort the policy list by "job function" or search for and attach the **"PowerUserAccess"** policy to the group
      - add the **"IAMReadOnlyAccess"** policy as well. We'll need this to verify our credentials at the command line.
    - now, there is User groups name "power-users" which have "PowerUserAccess" and "IAMReadOnlyAccess"

3. Create **Users**
    - navigate to Users
    - create a new user using your first name: "kelly-power-user"
    - select or create the group "power-users" (if you already have power-users group, just select it under User groups)
      - You can sort the policy list by "job function" or search for and attach the **"PowerUserAccess"** policy to the group
      - Add the **"IAMReadOnlyAccess"** policy as well. We'll need this to verify our credentials at the command line.

4. Create **Access Keys**
    - navigate to Users
    - click "kelly-power-user"
    - navigate to the security credentials tab
    - create a new access key, save and copy your new secret access keys, or download the .csv
    - Note:
      - You may one day find yourself in this situation where you've lost your secret access key or it is compromised. In this scenario, you don't have to delete and recreate your entire user. You can just create new credentials (Access Key ID and Secret Key).

5. Configuration
    
    The next procedure will guide you in configuring your AWS CLI local environment to authenticate to AWS via the command line and any other programs that require access to your AWS resources through your account.

    Run this command to set up your default credentials. When prompted for your keys, use your newly made IAM power-user credentials.

    ```aws configure```

    Input json for your data format and the default us-east-1 for your region. You are free to use other regions so long as you remember to stay consistent with your region config during other exercises.

    You'll know it is working when you can get a similar output following the use of this command.

    ```aws iam get-user```
    ```
    {
        "User": {
            "Path": "/",
            "UserName": "kelly-power-user",
            "UserId": "AIDAZNBJJFR2PS6QQHFDF",
            "Arn": "arn:aws:iam::646479162484:user/kelly-power-user",
            "CreateDate": "2023-02-28T13:49:13Z",
            "Tags": [
                {
                    "Key": "AKIAZNBJJFR2AP2V5AHV",
                    "Value": "kelly-power-user-access-key"
                }
            ]
        }
    }
    ```
<br>

#### Credentials in Environment Variables
There are two types of local configuration files in ```$HOME/.aws```
  - config
    ```
    [default]
    region = us-east-1
    ```
  - credentials
    ```
    [default]
    aws_access_key_id = YOUR_ACCESS_KEY_ID
    aws_secret_access_key = YOUR_SECRET_ACCESS_KEY
    ```

Later on, we'll run code that requires our credentials to be present in environment variables.

Environment variables for these values override local config files:
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY