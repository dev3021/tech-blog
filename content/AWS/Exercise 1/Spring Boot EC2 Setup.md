+++
title = "Spring Boot EC2 Setup"
weight = 1
draft = false
+++

### Overview
This procedure will also work for other Sprint Boot apps you may have, you'll just have to be diligent about changing the code snippets presented to match your naming.

Our goal now is to deploy our g-autos Spring Boot application to AWS EC2.

![spring](../../../images/spring-boot-ec2-architecture.png?width=800px&height=350px)

<br>

### Uploading g-autos .jar archive to AWS S3
#### Option 1. Uploading g-autos .jar archive to AWS S3

1. Build the g-autos application to produce a jar archive.(```./gradlew build```). It will be found in /target as **g-autos-0.0.1-SNAPSHOT.jar**

2. Upload your g-autos-0.0.1-SNAPSHOT.jar archive to S3. **It does not have to be publicly accessible**

3. Name your bucket "kelly-g-autos"

4. Upload your file using the dialog or via drag-drop

<br>

#### Option 2. Upload g-autos .jar archive to S3 via AWS CLI

1. Build the g-autos application to produce a jar archive.(```./gradlew build```). It will be found in /target as **g-autos-0.0.1-SNAPSHOT.jar**

2. Create a bucket via the **aws cli s3api**. 

    ```aws s3api create-bucket --bucket "kelly-g-autos"```

3. Buckets created via the CLI do not have public access blocks on them. Put a public access block on it with the following command:
   
    ```aws s3api put-public-access-block --bucket "kelly-g-autos" --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"```


4. Now put the g-autos archive into S3 using the **aws cli s3api** The following command should work from the project root directory, opened in Cloud9. Again, replace the values with your own.

    ```aws s3api put-object --bucket "kelly-g-autos" --key "g-autos-0.0.1-SNAPSHOT.jar" --body ./target/g-autos-0.0.1-SNAPSHOT.jar```

</br>

### Launching a web server instance

1. Open the AWS Management Console  and select **EC2** from Services or use the search bar

2. Launch a new instance
 
    - **Names and tags**: "kelly-spring-boot"

    - **Select Amazon Machine Image (AMI)**: Amazon Linux 2
      - lightweight, default, comes prepackaged with the aws-cli

    - **Select instance type**: t2.micro (1 vRAM, 1 vCPU)

    - **Key pair**:
      - needed admin access
      - authenticated with a cryptographic public/private key pair
      - Create new key pair or select a key pair for which you have already downloaded the private key for. Use the .pem format.
   
    - **Network settings**:
      - Default VPC
      - Create a new security group named: "kelly-spring-boot"
        - Edit existing security group rule for SSH, port 22, select source as "Anywhere"
        - Add new security group rule:
          - Type: Custom TCP
          - Protocol: TCP (Auto selected)
          - Port Range: 8080
          - Source: Anywhere (Open to the public internet)
    - Configure storage: use default values
    - Advanced details: use default values

<br/>

### Connect to your EC2 instance
Just like you did with the EC2 instance launched for PostgreSQL, connect to your new web server instance in the same way, via SSH.

1. Navigate to the EC2 instance dashboard

2. Select the instance to connect to, "kelly-spring-boot"

3. **Connect** and follow the directions very carefully in the **SSH Client** tab.

4. Once you're connected, you should see your terminal prompt change:

    ```
    Last login: Fri Oct 21 18:23:02 2022 from 76.147.216.35

        __|  __|_  )
        _|  (     /   Amazon Linux 2 AMI
        ___|\___|___|

    https://aws.amazon.com/amazon-linux-2/
    13 package(s) needed for security, out of 16 available
    Run "sudo yum update" to apply all updates.
    [ec2-user@ip-172-31-83-48 ~]
    ```
5. Follow the terminal output's directions from EC2 and run system-wide package updates to the latest for the most secure and up-to-date tools.

    ```sudo yum update -y```

<br/>

### Installing Java on Amazon Linux 2

1. Next, we'll need a way to run our jar file. Let's install Java. We'll use the [Amazon's Java 11 install](https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/amazon-linux-install.html)  and specifically the headless version as we don't need the GUI dependencies.
   
    ```sudo yum install -y java-11-amazon-corretto-headless```

<br/>

### Retrieving from S3

1. While **connected to your web server EC2 instance via SSH**, this is the command that you'll need to pull your file from S3 via the AWS CLI pre-installed on your instance. Replace <BUCKET_NAME> and <FILE_NAME> with our details from your S3 uploaded archive:
  
    ```aws s3api get-object --bucket "<BUCKET_NAME>" --key "<FILE_NAME>" "<FILE_NAME>"```

   - A bucket named "kelly-g-autos" and a file named "g-autos-0.0.1-SNAPSHOT.jar" would be retrieved with this command:
  
    ```aws s3api get-object --bucket "kelly-g-autos" --key "g-autos-0.0.1-SNAPSHOT.jar" "g-autos-0.0.1-SNAPSHOT.jar"```

2. If you ran that command in your EC2 instance, you should see this error:

    ```Unable to locate credentials. You can configure credentials by running "aws configure".```

3. While it would indeed fix the issue to configure your AWS CLI in that instance, it's a bad practice to store credentials on an instance should it be breached. Instead, we're going to configure an instance profile with a policy that allows it to access S3 in the AWS Management Console.

<br/>


### Attaching an Instance Profile to an EC2 Instance

#### Creating an Instance Role and S3 Policy

1. Navigate to **IAM**

2. Navigate to the **Roles** dashboard

3. **Create a new Role**

    - An IAM role is an identity you can create that has specific permissions with credentials that are valid for short durations. Roles can be assumed by entities that you trust.

    - Select **AWS Service** for your Trusted Entity Type

    - Select **EC2** for your Role's Use case
  
#### Creating a new Policy

1. In the next step, Add Permissions, **Create a new policy**
    - A policy defines the AWS permissions that you can assign to a user, group, or role. You can create and edit a policy in the visual editor and using JSON.

2. On this Create Policy step, click on the JSON tab. Read and comprehend this policy before pasting it into the management console:
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:ListBucket",
                    "s3:GetObject"
                ],
                "Resource": [
                    "arn:aws:s3:::kelly-g-autos",
                    "arn:aws:s3:::kelly-g-autos/*"
                ]
            },
            {
                "Effect": "Allow",
                "Action": "s3:ListAllMyBuckets",
                "Resource": "*"
            }
        ]
    }
    ```

3. The resource matches the ARN (Amazon Resource Number) of our S3 bucket named "kelly-g-autos" and the "*" wild card allows us to also access all the files in that bucket. **Match the resources with your actual bucket name.**
    - We also allow listing all our buckets in the account too.

4. Skip tags, click **Review**

5. **Name your policy**. Let's name it "kelly-list-get-s3-g-autos"

6. Click **Create Policy**


#### Adding the Policy to your Role

1. Go back to your tab where you wanted to Add Permissions to your new Role

2. Refresh the list. If it takes you back a step, just move forward to the next step.

3. You should now see your newly created Policy, "kelly-list-get-s3-g-autos"

4. Select the policy and click Next

5. Name your Role "kelly-ec2-list-get-s3-g-autos"

6. Click Create Role at the bottom

Now that we have our Instance role with a policy that allows us to grab objects from our S3 bucket, we need to attach it to our web server EC2 instance.


#### Attaching an Instance Role to an EC2 Instance

1. Navigate to your web server EC2 instance

2. Select your webserver instance and select the **Actions** dropdown

3. Select **Security** and then **Modify IAM Role**
On this page, select your "kelly-ec2-list-get-s3-g-autos" role and save.

<br>


### Retrieving from S3
1. If you are not already, connect to your instance via SSH

2. Now run this command to verify that your role and policy attachment worked properly

    ```aws s3api list-buckets```

3. If that worked, you should be able to download the file from s3 too.

    ```aws s3api get-object --bucket "firstname-g-autos" --key "g-autos-0.0.1-SNAPSHOT.jar" "g-autos-0.0.1-SNAPSHOT.jar"```

3. Run the command ```ls``` and you should see your file listed like this
    ```[ec2-user@ip-172-31-1-49 ~]$ ls
    g-autos-0.0.1-SNAPSHOT.jar
    ```
<br/>

### Running your Spring Boot App

Now that you have Java installed and you have your jar file. You can run the application.

```java -jar g-autos-0.0.1-SNAPSHOT.jar```

<br>

#### Supplying updated database host address to the Spring Boot app
At this point, you'll probably receive a Spring Boot error because it cannot connect to the PostgreSQL instance. The app's config still points its database connection to **localhost** in the **application.properties** file

We have two options, rebuild the application with the new host, upload to S3, and pull it to our EC2 instance **OR** we can override the present application's config values with an environment variable.


Run the following command with the proper values:

```export DB_HOST=PRIVATE_IP_OF_POSTGRES_EC2_INSTANCE```

Run the application again with:

```java -jar g-autos-0.0.1-SNAPSHOT.jar```

<br/>

### Testing the EC2 web server
It's time to test your running application in the browser.

1. Navigate to your instance in the AWS Console and click on its ID.

2. Under the section labeled **Public IPv4 Address** OR **Public IPv4 DNS**, copy the address to your address bar and add ":8080" to the end of it so it looks like this: "ec2-3-101-17-67.us-west-1.compute.amazonaws.com:8080"

3. You should see the Spring server Whitelabel Error Page

4. You can now test your API using the path **"/autos"**

5. Similar steps can be taken to test the API from the command line using cURL or with the app Postman.

<br/>
