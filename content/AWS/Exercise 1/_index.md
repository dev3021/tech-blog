+++
title = "Exercise 1"
date = 2022-09-20T09:45:34-04:00
weight = 5
chapter = true
pre = "<b></b>"
+++

### PostgreSQL & Spring Boot EC2 Setup

- Project Repository: [g-autos](https://gitlab.com/cicd-training1/tf-g-autos)

#### Steps

1. Install a PostgreSQL DB server on an AWS EC2 instance
2. Run a Spring Boot Maven application on AWS EC2
3. Make a call to AWS S3 from EC2
4. Configure EC2 Security Groups

### Index
{{% children %}}
