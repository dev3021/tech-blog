+++
title = "View"
weight = 1
draft = false
+++
<!--when the draft = true => it wont' show on the website 
weight will decide the order of menu (the lower the first )-->


### 1. Create Welcome Page
- resources/static/index.html
```
<!DOCTYPE HTML>
<html>
<head>
    <title>Hello</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
Hello
<a href="/hello">hello</a>
</body>
</html>
```
- Welcome Page feature provided by Spring Boot. Once static/index.html is uploaded, the Welcome page functionality is provided.
https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/html/spring-boot-features.html#boot-features-spring-mvc-welcome-page
- thymeleaf: https://www.thymeleaf.org/
- Spring Tutorial: https://spring.io/guides/gs/serving-web-content/
- Spring Manual: https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/html/spring-boot-features.html#boot-features-spring-mvc-template-engines

### 2. Project Build with Spring Initizalizr
```
@Controller
public class HelloController {
 @GetMapping("hello")
 public String hello(Model model) {
 model.addAttribute("data", "hello!!");
 return "hello";
 }
}
```
- resources/templates/hello.html
```
<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
  <title>Hello</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<p th:text="'Hello. ' + ${data}" >Hello. there</p>
</body>
</html>
```
- https://start.spring.io
- Select Projects
  - Project: Gradle Project
  - Spring Boot: 2.3.x
  - Language: Java
  - Packaging: Jar
  - Java: 11
- Project Metadata
  - groupId: hello
  - artifactId: hello-spring
- Dependencies: Spring Web, Thymeleaf

![spring](../../../images/view.png?width=800px&height=350px)
    
- Click Generate, and download zip file
- unzip and open project with IntelliJ

{{% notice note %}}
Gradle Global Setting: **build.gradle**
{{% /notice %}}

```
plugins {
	id 'org.springframework.boot' version '2.7.5'
	id 'io.spring.dependency-management' version '1.0.15.RELEASE'
	id 'java'
}

group = 'hello'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '11'

repositories {
	mavenCentral()
}

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	implementation 'org.springframework.boot:spring-boot-starter-web'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
}

tasks.named('test') {
	useJUnitPlatform()
}
```

### 3. Testing
  - start main class
  - go to http://localhost:8080


{{% notice note %}}
Run Java directly instead of IntelliJ Gradle. The default setting for recent IntelliJ version is to run through Gradle. This will slow down execution. If you change it as follows, iti is executed directly in Java and the execution speed is faster.
Preferences Build, Execution, Deployment Build Tools Gradle
Build and run using: Gradle IntelliJ IDEA
Run tests using: Gradle IntelliJ IDEA
{{% /notice %}}

![intellij-grandle-settings](../../../images/intellij-grandle-settings.png?width=700px&height=350px)

> Gradle은 의존관계가 있는 라이브러리를 함께 다운로드 한다.
스프링 부트 라이브러리
spring-boot-starter-web
spring-boot-starter-tomcat: 톰캣 (웹서버)
spring-webmvc: 스프링 웹 MVC
spring-boot-starter-thymeleaf: 타임리프 템플릿 엔진(View)
spring-boot-starter(공통): 스프링 부트 + 스프링 코어 + 로깅
spring-boot
spring-core
spring-boot-starter-logging
logback, slf4j
테스트 라이브러리
spring-boot-starter-test
junit: 테스트 프레임워크
mockito: 목 라이브러리
assertj: 테스트 코드를 좀 더 편하게 작성하게 도와주는 라이브러리
spring-test: 스프링 통합 테스트 지원