+++
title = "Vue Router"
weight = 3
draft = false
+++

### Vue Router

- router/index.js
  - Method1:
     - import HomeView from '../views/HomeView.vue' 
     - component: HomeView
     - if you use method1 => all compied codes go under app.js
![Profile](../../images/method-1.png?width=550px&height=450px)
  - Method2:
     - component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
     - if you use method2 => about.js, and initiator is app.js
![Profile](../../images/method-2.png?width=550px&height=350px)

    ```js
    import { createRouter, createWebHistory } from 'vue-router'
    import HomeView from '../views/HomeView.vue' // method1

    // You cannot have the same name for routes
    // There are two ways to use component method1, method2
    const routes = [
    {
        path: '/',
        name: 'home',
        //method 1
        component: HomeView
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        // method2
        // if you change webpackChunkName: "about" => "somethingelse", it create a chunk as somethingelse.js
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
    ]

    const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
    })

    export default router


- sidenote: setting eslint, prettier
  - create .prettierrc file under project folder
    ```js
        { 
            "semi": false, 
            "bracketSpacing": true,
            "singleQuote": true,
            "useTabs": false,
            "trailingComma": "none",
            "printWidth": 80
        }
  - add `"rules":{"space-before-function-paren": "off"}` in package.json file