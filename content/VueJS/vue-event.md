+++
title = "Vue Event"
weight = 5
draft = false
+++

### Vue Event
- create folder views/2_event

#### Event Click
1. create EventClickView.vue(https://github.com/kellylee-dev/Vue-study/blob/master/src/views/2_event/EventClickView.vue) 
   - view에서는 on 대신에 @붙인다 onClick => **@click**
2. add in router/index.js file
    ```js
    {
      path: '/event/click',
      name: 'EventClickView',
      component: () =>
        import(
          /* webpackChuncName: "event", webpackPrefetch:true */ '../views/2_event/EventClickView.vue'
        )
    }
    ```

#### Event Change
1. create [EventChangeView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/2_event/EventChangeView.vue)
   - 이중셀렉트: 처음 셀렉트한 결과값에 따라서 두번째 셀렉트 옵션이 달라진다
   - **@change** 사용
2. add in router/index.js file


#### Event Key
1. create [EventKeyView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/2_event/EventKeyView.vue)
   - Method1: **@keyup="checkEnter($event)"** 사용
   - Method2: **@keyup.enter="doSearch"** 더 간단하게 바로 doSearch메소드 호출가능
2. add in router/index.js file
