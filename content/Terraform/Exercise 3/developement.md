+++
title = "Developement"
weight = 2
draft = false
+++

#### Frontend Development
- sudo npm install -g @vue/cli
- cd kelly-project
- vue create frontend
- npm run server
- localhost:80
- create vue.config.js

#### Backend Development
- npm install -g express-generator
- express --view=pug backend => creating backend folder
- npm install
- npm start
- localhost:3000
