+++
title = "Docker Deployment"
weight = 6
draft = false
+++

### Frontend
  - gitlab
    - ``docker login registry.gitlab.com`` (get Personal Access Token)
    - Dockerfile 
      - **ENV VUE_APP_BACKEND_ADDRESS=localhost**
      - **EXPOSE 80**
    - ``docker build -t registry.gitlab.com/cicd-training1/kelly-project/frontend:1.0.0 . --no-cache``
    - ``docker run -it -p 80:80 --rm --name frontend registry.gitlab.com/cicd-training1/kelly-project/frontend:1.0.0`` 
    - if you exit the terminal, it will stop, but if you add **-d**=> still works after you close the terminal
    - ``docker run -it -d -p 80:80 --rm --name frontend registry.gitlab.com/cicd-training1/kelly-project/frontend:1.0.0`` 

  - docker Hub
    - docker build -t kellylee0475/frontend:1.0.0 . --no-cache
    - docker push kellylee0475/frontend:1.0.0
  
  - Note:
    - docker run -it -p **5000:80** --rm --name frontend2 kellylee0475/frontend:1.0.0 (**localhost:5000**)
    - docker run -it -p **81:80** --rm --name frontend1 kellylee0475/frontend:1.0.0 (**localhost:81**)
    - frontend is running on 80 (nignx) host is just 5000 or 81

### Backend 
  - gitlab
    - docker login registry.gitlab.com (get Personal Access Token)
    - Dockerfile 
      - **ENV DB_HOST="kelly-tf-database.ckdatbejuaih.us-east-1.rds.amazonaws.com"**
      - **EXPOSE 4000**
    - ``docker build -t registry.gitlab.com/cicd-training1/kelly-project/backend:1.0.0 . --no-cache``
    - ``docker run -it -p 4000:4000 --rm --name backend registry.gitlab.com/cicd-training1/kelly-project/backend:1.0.0``
    - http://localhost:4000/api/list => this works
    - if you exit the terminal, it will stop, but if you add -d => still works after you close the terminal
    - ``docker run -it -d -p 4000:4000 --rm --name backend registry.gitlab.com/cicd-training1/kelly-project/backend:1.0.0``
  
