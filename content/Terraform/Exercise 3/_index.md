+++
title = "Exercise 3"
date = 2022-09-20T09:45:34-04:00
weight = 5
chapter = true
pre = "<b></b>"
+++

## Terraform with AWS

##### Project Repository: 
- [Kelly Project](https://gitlab.com/cicd-training1/kelly-project)
- [Kelly Project Database](https://gitlab.com/cicd-training1/kelly-project-database)

##### Project Description: 
- Deploying Vue.js(Frontend), Express.js(Backend) in AWS EC2 and PostgreSQL(Database) in AWS RDS with Terraform


### Index
{{% children %}}
