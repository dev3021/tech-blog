+++
title = "Terraform Deployment CI/CD"
weight = 5
draft = false
+++

### Run Database Pipeline
- This pipeline will create RDS in AWS, and create a table

    ```yaml
    stages:
      - tf-db-validate
      - tf-db-test
      - tf-db-build
      - tf-db-deploy
      - create-db-table
      - tf-db-destroy

    ##########################################################
    ######################## Variables #######################
    ##########################################################

    variables:
      PG_IMAGE: postgres
      TF_IMAGE: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.1:v0.43.0"
      TF_ROOT: ${CI_PROJECT_DIR}  # The relative path to the root directory of the Terraform project
      TF_STATE_NAME: default      # The name of the state file used by the GitLab Managed Terraform state backend


    ##########################################################
    ###################### Terraform DB ######################
    ##########################################################

    tf-db-fmt:
      stage: tf-db-validate
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - gitlab-terraform fmt
      allow_failure: true
      # when: manual


    tf-db-validate:
      stage: tf-db-validate
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - gitlab-terraform validate
      allow_failure: true
      # when: manual


    tf-db-build:
      stage: tf-db-build
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - pwd
        - gitlab-terraform plan
        - gitlab-terraform plan-json
      resource_group: ${TF_STATE_NAME}
      artifacts:
        paths:
          - ${TF_ROOT}/plan.cache
        reports:
          terraform: ${TF_ROOT}/plan.json
      # allow_failure: true
      # when: manual


    tf-db-deploy:
      stage: tf-db-deploy
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - gitlab-terraform apply
        - export DB_ADDRESS=$(terraform output db_address)
        - echo $DB_ADDRESS
        - echo "DB_ADDRESS=$(terraform output db_address)" >> build.env # passing to the next job to create DB table
      resource_group: ${TF_STATE_NAME}
      artifacts:
        reports:
          dotenv: build.env
      # when: manual
      # allow_failure: true
    

    ##########################################################
    ######################### DB Table #######################
    ##########################################################

    create-db-table:
      stage: create-db-table
      image: $PG_IMAGE
      variables:
        # https://gitlab.com/gitlab-examples/postgres/-/blob/master/.gitlab-ci.yml
        POSTGRES_DB: postgres
        POSTGRES_USER: autos
        POSTGRES_PASSWORD: autos123
        POSTGRES_QUERY: CREATE TABLE login (firstname VARCHAR(100), lastname VARCHAR(100));
      script:
        - export PGHOST=`echo $DB_ADDRESS | tr -d '"'`
        - echo $PGHOST
        - export PGPASSWORD=$POSTGRES_PASSWORD
        - echo $POSTGRES_QUERY
        - psql -U "$POSTGRES_USER" -d "$POSTGRES_DB"  -c "$POSTGRES_QUERY"
      when: manual
      

    ##########################################################
    ###################### Terraform DB ######################
    ##########################################################

    tf-db-destroy:
      stage: tf-db-destroy
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - gitlab-terraform destroy
      resource_group: ${TF_STATE_NAME}
      when: manual
    ```

### Run kelly-project Pipeline
- This will deploy frontend and backend in AWS EC2

    ```yaml
    stages:
      - tf-validate
      - tf-test
      - tf-build
      - tf-deploy
      - tf-cleanup


    ##########################################################
    ######################## Variables #######################
    ##########################################################

    variables:
      DB_HOST:
        value: kelly-tf-database.ckdatbejuaih.us-east-1.rds.amazonaws.com
        description: "specify RDS address"
      PG_IMAGE: postgres
      TF_IMAGE: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.1:v0.43.0"
      TF_ROOT: ${CI_PROJECT_DIR}  # The relative path to the root directory of the Terraform project
      TF_DB: ${CI_PROJECT_DIR}/db
      TF_STATE_NAME: default      # The name of the state file used by the GitLab Managed Terraform state backend


    ##########################################################
    ######################## Terraform  ######################
    ##########################################################

    tf-fmt:
      stage: tf-validate
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - gitlab-terraform fmt
      allow_failure: true


    tf-validate:
      stage: tf-validate
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - gitlab-terraform validate
      allow_failure: true


    tf-build:
      stage: tf-build
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - echo $DB_HOST
        - export TF_VAR_RDS_DB_HOST=$DB_HOST #To use in main.tf for EC2 Backend connection to DB
        - echo "TF_VAR_RDS_DB_HOST=$DB_HOST" >> build.env
        - gitlab-terraform plan
        - gitlab-terraform plan-json
      resource_group: ${TF_STATE_NAME}
      artifacts:
        paths:
          - ${TF_ROOT}/plan.cache
        reports:
          terraform: ${TF_ROOT}/plan.json
          dotenv: build.env
          

    tf-deploy:
      stage: tf-deploy
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - gitlab-terraform apply
        - echo $TF_VAR_RDS_DB_HOST
      resource_group: ${TF_STATE_NAME}


    tf-cleanup:
      stage: tf-cleanup
      image: $TF_IMAGE
      script:
        - cd "${TF_ROOT}"
        - echo $TF_VAR_RDS_DB_HOST
        - gitlab-terraform destroy
      resource_group: ${TF_STATE_NAME}
      when: manual

    ```