+++
title = "Project Description"
weight = 1
draft = false
+++

### kelly-project

- Vue.js + Node.js + Express + PostgreSQL
  - Frontend: Vue.js, AWS EC2
  - Backend: Express.js, AWS EC2
  - Database: Postgresql, AWS RDS

![infra](../../../images/kelly-project-description1.png)


- Reference: https://www.bezkoder.com/vue-node-express-postgresql/


### Deployment
![infra](../../../images/kelly-project-description2.png)

### Reference
- https://plainenglish.io/blog/deploying-a-nodejs-application-in-aws-ec2
- https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html
- https://jonathans199.medium.com/how-to-deploy-node-express-api-to-ec2-instance-in-aws-bc038a401156
- https://thucnc.medium.com/deploy-a-vuejs-web-app-with-nginx-on-ubuntu-18-04-f93860219030
- https://www.ankursheel.com/blog/upload-multiple-files-aws-s3-terraform

