+++
title = "Solution"
weight = 2
draft = false
+++


1. Create a new project

2. Initialize the project as a git repository: ```git init```.

3. Create a **.gitignore** file with these contents .gitignore - Terraform template

4. Create a /build/libs directory in your project and copy over your Spring Boot application archive file (.jar). You may need to build the project to produce a .jar file.

5. Create a **main.tf** file and paste in these contents to get started - make a note to set your region as intended

      ```tf
        terraform {
          required_providers {
              aws = {
              source  = "hashicorp/aws"
              version = "~> 4.0"
              }
          }
        }

        provider "aws" {
        region = "us-east-1"
        }
      ```

6. Initialize the Terraform project with ```terraform init```

7. create **AWS S3 Bucket**

      ```tf
      resource "aws_s3_bucket" "kelly-s3-bucket" {
        bucket = "kelly-tf-s3-bucket" // name of the bucket

        tags = {
          Name        = "My bucket"
          Environment = "Dev"
        }
      }
      ```


8. create **AWS S3 Object**

      ```tf
      resource "aws_s3_object" "kelly-s3-object" {
        bucket = aws_s3_bucket.kelly-s3-bucket.id // kelly-s3-bucket is referring to aws_s3_bucket that I just created above
        key    = "spring-boot-snpashpt-jar-1" // Name of the object
        source = "./build/libs/zoo-tdd-demo-0.0.1-SNAPSHOT.jar" //the location in local
      }
      ```



9. create **Security Group** for EC2 Instance

    - Ingress rules
      - **cidr_blocks** represent the **"source"** of the inbound traffic allowed. This may be set to "0.0.0.0/0" (Anywhere IPv4) for public rules

      - represent rules governing "inbound" rules (as seen in the AWS Management Console)
      > if you put
        from_port = 8070
        to_port = 8080
        it will show in AWS console Port range: 8070 - 8080

    - Engress rule
      - necessary for outbound traffic to flow
      - a "-1" protocol represents ALL protocols - meaning that all communications using any protocols are allowed


      ```tf
      resource "aws_security_group" "kelly-security-group" {
        name        = "kelly-tf-security-group"
        description = "Allow Spring default port and SSH inbound"

        ingress {
          description      = "HTTP 8080 from anywhere"
          from_port        = 8080
          to_port          = 8080
          protocol         = "tcp"
          cidr_blocks      = ["0.0.0.0/0"]
        }
        ingress {
          description      = "SSH from anywhere"
          from_port        = 22
          to_port          = 22
          protocol         = "tcp"
          cidr_blocks      = ["0.0.0.0/0"]

        }
        egress {
          from_port        = 0
          to_port          = 0
          protocol         = "-1"
          cidr_blocks      = ["0.0.0.0/0"]
          ipv6_cidr_blocks = ["::/0"]
        }

        tags = {
          Name = "kelly-tf-security-group-tag" //this goes to Name section in console
        }
      }
      ```

10. create **IAM Role**

      ```tf
      resource "aws_iam_role" "kelly-tf-role" {
        name = "kelly-tf-role-list-get-s3"

        assume_role_policy = jsonencode({
          Version = "2012-10-17"
          Statement = [
            {
              Action = "sts:AssumeRole"
              Effect = "Allow"
              Sid    = ""
              Principal = {
                Service = "ec2.amazonaws.com"
              }
            },
          ]
        })

        tags = {
          tag-key = "tag-kelly"
        }
      }
      ```


11. create **IAM Role Policy**
    - This resource creates an IAM Role Policy that can be ```defined inline```.
    - This way, **this policy is only available for this specific role**. If you go to AWS console > IAM > Policy, it won't show up.
    - if you want to show up in policy section, you can create **aws_iam_role_policy_attachment** instead, and attach that to the role.


      ```tf
      resource "aws_iam_role_policy" "kelly-tf-role-policy" {
        name = "kelly-tf-role-policy"
        role = aws_iam_role.kelly-tf-role.id

        # Terraform's "jsonencode" function converts a
        # Terraform expression result to valid JSON syntax.
        policy = jsonencode({
          Version = "2012-10-17"
          Statement = [
            {
                  "Effect": "Allow",
                  "Action": [
                      "s3:ListBucket",
                      "s3:GetObject"
                  ],
                  "Resource": [
                      "arn:aws:s3:::${aws_s3_bucket.kelly-s3-bucket.id}",
                      "arn:aws:s3:::${aws_s3_bucket.kelly-s3-bucket.id}/*"
                  ]
              },
              {
                  "Effect": "Allow",
                  "Action": "s3:ListAllMyBuckets",
                  "Resource": "*"
              },
          ]
        })
      }
      ```

12. create **aws_iam_instance_profile**

    - **This resource represents the IAM instance profile that will be attached to your EC2 instance.**
      - name - this argument/attribute will need to be provided to the aws_instance resource
      - role - should be set to the name of the aws_iam_role resource
      - this does not exisit in AWS. if you want to delete this, you need to use AWS CLI.

      ```tf
      resource "aws_iam_instance_profile" "kelly-tf-iam-instance-profile" {
        name = "kelly-tf-iam-instance-profile"
        role = aws_iam_role.kelly-tf-role.name
      }
      ```


13. create **EC2 Instance**

  - ami AMIs are specific to regions. The Amazon Linux 2 AMI has different IDs depending on the region. Use one of the following methods to source this ID.
  - ```key_name``` - Create a key pair in the AWS Management Console at the EC2 dashboard or reference a key you have already created. This argument creates the EC2 instance with the capability of connecting to it via SSH and authenticating with your private key. If you do NOT include this argument, the instance will still be created but you will have no way to administer the instance by connecting to it. Pairs switching after this value is set will need to provide an updated key name. 

  - ```security_groups``` - attach the security group to EC2
  - ```iam_instance_profile``` - provide the IAM instance profile name so that the EC2 instance will have permission to get objects from your S3 bucket


      ```tf
      resource "aws_instance" "kelly-tf-ec2-instance" {
        ami                     = "ami-0dfcb1ef8550277af"
        instance_type           = "t2.micro"
        key_name             = "kelly-keypair" // this is my kelly-keypair.pem file
        security_groups = [aws_security_group.kelly-security-group.name]
        iam_instance_profile = aws_iam_instance_profile.kelly-tf-iam-instance-profile.name
        tags = {
          Name = "kelly-tf-ec2-instance"
        }

        user_data = <<EOF
        #! /bin/bash
        sudo yum update -y
        sudo yum install -y java-11-amazon-corretto-headless
        aws s3api get-object --bucket "${aws_s3_bucket.kelly-s3-bucket.id}" --key "${aws_s3_object.kelly-s3-object.key}" "${aws_s3_object.kelly-s3-object.key}"
        java -jar ${aws_s3_object.kelly-s3-object.key}
        EOF
      }
      ```


14. **Testing**

    - ```http://EC2_Public_IPv4_DNS:8080/``` (ex: http://ec2-34-205-171-79.compute-1.amazonaws.com:8080/)  => whitelabel Error
    - ```http://EC2_Public_IPv4_DNS:8080/api/animals```

