+++
title = "Asynchronous JavaScript: Promises, Async/wait and AJAX"
weight = 18
draft = false
+++

### Asynchronous JavaScript, AJAX and APIs

- Synchronous Code
  - Most code is synchornous
  - Sychronous code is executed line by line
  - Each line of code waits for previous line to finish
  - Long-running operations block code execution

- Asynchronous Code
  - Asynchronous code is executed after a task that runs in the "background" finishes
  - Asynchronous code is non-blocking
  - Execution doesn't wait for an asynchronous task to finish its work
  - Callback functions alone do NOT make code asynchronous
  - Example 1: Timer with callback
    ```js
    const p = document.querySelector('.p');
    //Callback will run after timer
    setTimeout(function(){
        p.textContent = 'My name is Jonas!';
    }, 5000);
    p.style.color = 'red';
    ```
  - Example 2: Asynchronous image loading with event and callback
    ```js
    const img = document.querySelector('.dog');
    //setting resource attribute was implemented in JavaScript as Asynchronous(we don't wait till big image gets loaded)
    img.src = 'dog.jpg';
    img.addEventListener('load', function(){
      img.classList.add('fadeIn');
    });
    p.style.width='300px';
    ```
  - Other Examples: Geolocatioin API or AJAX calls

- AJAX
  - <b>A</b>synchronous <b>J</b>avaScript <b>A</b>nd <b>X</b>ML: Allows us to communicate with remote web servers in an asynchronous way. With AJAX calls, we can <b>request data</b> from web servers dynamically.
  - Client(browser) => Request(GET/POST/etc): Asking for some data => Web Server(Usually a web API)
  - Web Server => Response: Sending data back => Client(browser)

- API
  - <b>A</>pplication <b>P</b>rogramming <b>I</b>terface: Piece of software that can be used by another piece of software, in order to allow <b>applications to talk to each other</b>
  - There are be many types of APIs in web developement
    - DOM API, Geolocation API, Own Class API, Online API
  - Online API = Web API = API : Application running on a server, that receives requests for data, and sends data back as response
  - We can build our own web APIs(requires back-end develpement) or use 3rd-party APIs

- XML: data format => no longer used
- JSON: data format => Most popular API data format


### AJAX Call: XMLHttpRequest

### How the Web Works: Requests and Responses

### Callback Hell

### Promises and the Fetch API