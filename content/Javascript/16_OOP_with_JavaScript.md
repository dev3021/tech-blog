+++
title = "OOP with JavaScript"
weight = 16
draft = false
+++

### OOP
- OOP is a programming paradigm based on the concept of objects.
- Objects may contain data(properties) and code(methods). By using objects, we pack data and the corresponding behavior into one block.
- In OOP, objects are self-contained pieces/blocks of code.
- Objects are building blocks of applications, and interact with one another.
- Interactions happen through a public interface(API): methods that the code outside of the object can access and use to communicate with the object.
- OOP was developed with the goal of organizing code, to make it more flexible and easier to maintain.
  
### The 4 Fundamental OOP Principles
- Abstraction
  - Ignoring or hiding details that don't matter, allowing us to get an overview perspective of the thing we're implementing, instead of messing with details that don't really matter to our implementation.
- Encapsulation
  - Keeping properties and methods private inside the class, so they are not accessible from outside the class. Some methods can be exposed as a public interface(API)
- Inheritance
  - Making all properties and methods of a certain class available to a child class, forming a hierarchical relationship between classes. This allows us to reuse common logic and to model real-world relationships.
- Polymorphism
  - A child class can overwrite a method it inherited from a parent class

<br>

### Constructor Functions and the new Operator


- How the new operator works?
  1. An emtpy object {} is created
  2. this keyword in constructor function call is et to the new object, this = {}
  3. The new object is linked to the constructor function's prototype property
  4. The new object is returned from the constructor function call
  
    
    ```js

    const Person = function(firstName, birthYear){
        console.log(this);//Person {}
        
        this.firstName = firstName;
        this.birthYear = birthYear;

        //This is not a good way => bad performance
        //Never create a function inside of constructor!
        //Use prototype instead
        this.calcAge = function(){
            console.log(2023-this.birthYear);
        }
    }

    //using new operator
    const jonas = new Person('Jonas',1991);
    console.log(jonas);//Person {firstName: 'Jonas', birthYear: 1991}
    const matilda = new Person('Matilda', 2017);
    console.log(matilda);//Person {firstName: 'Matilda', birthYear: 2017}

    const jay = 'Jay';
    console.log(jonas instanceof Person);//true
    console.log(jay instanceof Person);//false

    ```

<br>

### Prototypes
```js
const Person = function(firstName, birthYear){
    console.log(this);//Person {}
    
    this.firstName = firstName;
    this.birthYear = birthYear;
}

const jonas = new Person('Jonas',1991);

//Prototypes
console.log(Person.prototype);//{constructor: ƒ} (calcAge, constructor(firstName,birtYear))

//Instead of creating a function inside of constructor, do this.
Person.prototype.calcAge = function(){
    console.log(2023 - this.birthYear);
}

jonas.calcAge();//32

//prototype of jonas
console.log(jonas.__proto__);//{calcAge: ƒ, constructor: ƒ}
console.log(jonas.__proto__ === Person.prototype);//true

console.log(Person.prototype.isPrototypeOf(jonas));//true
console.log(Person.prototype.isPrototypeOf(Person));//false

//Setting property
Person.prototype.species = 'Homo Sapiens';
console.log(jonas.species);//Homo Sapiens

//check if it's own property or prototype property
console.log(jonas.hasOwnProperty('firstName'));//true
console.log(jonas.hasOwnProperty('species'))//false
```

<br>

### Prototypal Inheritance and The Prototype Chain

- Prototype Chain: Series of links between objects, linked through prototypes(similar to the scope chain)
  
![prototype](../images/prototype.png?width=800px&height=350px)

```js
const Person = function(firstName, birthYear){
    console.log(this);//Person {}
    
    this.firstName = firstName;
    this.birthYear = birthYear;
}

const jonas = new Person('Jonas',1991);

//Instead of creating a function inside of constructor, do this.
Person.prototype.calcAge = function(){
    console.log(2023 - this.birthYear);
}
//Setting property
Person.prototype.species = 'Homo Sapiens';


console.log(jonas.__proto__);//{species: 'Homo Sapiens', calcAge: ƒ, constructor: ƒ}
//Object.prototype(top of prototype chain)
console.log(jonas.__proto__.__proto__);//{constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ,...}
console.log(jonas.__proto__.__proto__.__proto__);//null

console.log(Person.prototype.constructor);
/*ƒ (firstName, birthYear){
    console.log(this);//Person {}
    
    this.firstName = firstName;
    this.birthYear = birthYear;
}*/

console.dir(Person.prototype.constructor);//ƒ Person(firstName, birthYear)

//new Array is the same as using [ ]
const arr = [3,4,5,6,7];
//showing all array methods
console.log(arr.__proto__);//[constructor: ƒ, at: ƒ, concat: ƒ, copyWithin: ƒ, fill: ƒ, …]
console.log(arr.__proto__ === Array.prototype);//true

```

<br>

### Prototypal Inheritance on Built-In Objects
- Fun experiement but shouldn't use this way
    ```js
    const arr = [3,4,5,6,7,3,6];

    //Adding the new method to the prototype property of the array constructor
    //all Array will inherit this method
    Array.prototype.unique = function(){
        return [...new Set(this)];
    }

    //calling the method that we created
    console.log(arr.unique());//[3, 4, 5, 6, 7]
    ```

- Car Exercise 1: with Constructor Function
     ```js
    //1. Use a constructor function to implement a Car.
    //   A car has a make and a speed property.
    //   The speed property is the current speed of the car in km/h
    const Car = function(make, speed){
        this.make = make;
        this.speed = speed;
    }

    //2. Implement an 'accelerate' method that will increase the car's speed by 10, and log the new speed to the console
    Car.prototype.accelerate = function(){
        this.speed += 10;
        console.log(`${this.make} is going at ${this.speed}km/h`);
    }

    //3. Implement a 'brake' method that will decrease the car's speed by 5, and log the new speed to the console
    Car.prototype.brake = function(){
        this.speed = this.speed - 5;
        console.log(`${this.make} is going at ${this.speed}km/h`);
    }

    //4. create 2 car objects and experiment with calling 'accelerate' and 'brake' multiple times on each of them
    const car1 = new Car('BMW', 120);
    const car2 = new Car('Mercedes', 95);

    car1.accelerate();//BMW is going at 130km/h
    car2.brake();//Mercedes is going at 90km/h
    ```

<br>


### ES6 Classes

- Classes are NOT hoisted
- Classes are first-class citizens
- Classes are executed in strict mode 
  
```js
//You can use either Class Expression or Class Declaration
//1. Class Expression
const PersonClExpression = class {

}

//2. Class Declaration
class PersonCl {
    constructor(firstName, birthYear){
        this.firstName = firstName;
        this.birthYear = birthYear;
    }

    // Option 1. write a method inside of class, outside of constructor
    // this method will be added to .prototype property
    calcAge(){
        console.log(2023 - this.birthYear);
    }

}

const jessica = new PersonCl('Jessica', 1996);
console.log(jessica);//PersonCl {firstName: 'Jessica', birthYear: 1996. Prototype(calcAge)}

jessica.calcAge();//27

console.log(jessica.__proto__ === PersonCl.prototype);//true

// Option 2. adding method manually to the prototype
PersonCl.prototype.greet = function(){
    console.log(`Hey ${this.firstName}`);
}

jessica.greet();//Hey Jessica
```

<br>

### Setters and Getters

```js
const account = {
    owner: 'jonas',
    movements: [200, 530, 120, 300],

    //getter
    get latest(){
        return this.movements.slice(-1).pop();
    },
    //setter
    set latest(mov){
        this.movements.push(mov);
    },

};

//call it like a property not latest()
console.log(account.latest);//300

//account.latest(50) => X
//property not a method
account.latest = 50;
console.log(account.movements);//[200, 530, 120, 300, 50]
```

<br>

### Object.create

- This is not really used in real world

    ```js
    const PersonProto = {
        calcAge(){
            console.log(2023 - this.birthYear);
        },
        //Option 2
        init(firstName, birthYear){
            this.firstName = firstName;
            this.birthYear = birthYear;
        }
    };

    const steven = Object.create(PersonProto);
    console.log(steven);
    //Option 1.
    steven.name = 'Steven';
    steven.birthYear = 2002;
    steven.calcAge();//21

    console.log(steven.__proto__ === PersonProto);//true

    const sarah = Object.create(PersonProto);
    //Option 2
    sarah.init('Sarah', 1979);
    sarah.calcAge();//44
    ```

- Car Exercise 2: with EX6 Classes
    ```js
    //########### Previous Exercise with Prototpye
    const Car = function(make, speed){
        this.make = make;
        this.speed = speed;
    }

    //2. Implement an 'accelerate' method that will increase the car's speed by 10, and log the new speed to the console
    Car.prototype.accelerate = function(){
        this.speed += 10;
        console.log(`${this.make} is going at ${this.speed}km/h`);
    }

    //3. Implement a 'brake' method that will decrease the car's speed by 5, and log the new speed to the console
    Car.prototype.brake = function(){
        this.speed = this.speed - 5;
        console.log(`${this.make} is going at ${this.speed}km/h`);
    }

    //4. create 2 car objects and experiment with calling 'accelerate' and 'brake' multiple times on each of them
    const car1 = new Car('BMW', 120);
    const car2 = new Car('Mercedes', 95);

    car1.accelerate();//BMW is going at 130km/h
    car2.brake();//Mercedes is going at 90km/h
        

    //########### Recreate using an ES6 class
    //1. Convert Prototype to Class
    class CarCl {
        constructor (make, speed){
            this.make = make;
            this.speed = speed;
        }

        //Car.prototype.accelerate => accelerate : don't need a prototype, that's the whole point of using class
        accelerate(){
            this.speed += 10;
            console.log(`${this.make} is going at ${this.speed}km/h`);
        }

        brake(){
            this.speed = this.speed - 5;
            console.log(`${this.make} is going at ${this.speed}km/h`);
        }
        
        //2. Add a getter called 'speedUS' which returns the current speed in mi/h(divide by 1.6)
        get speedUS(){
            return this.speed / 1.6;
        }

        //3. Add a setter called 'sppedUS' which sets the current speed in mi/h(but converts it to km/h before stroing the value, by multiplying the input by 1.6);
        set speedUS(speed){
            this.speed = speed * 1.6;
        }
    }

    //4. Create a new car and experiment with the accelerate and brake methods, and with the getter and setter.
    const ford = new CarCl('Ford',120);
    console.log(ford.speedUS);//75
    ford.accelerate();//Ford is going at 130km/h
    ford.brake();//Ford is going at 125km/h
    ford.speedUS = 50;
    console.log(ford);//CarCl {make: 'Ford', speed: 80}
    ```

<br>

### Inheritance Between "Classes": Construcutor Functions

```js
const Person = function(firstName, birthYear){
    this.firstName=firstName;
    this.birthYear=birthYear;
};

Person.prototype.calcAge = function(){
    console.log(2023-this.birthYear);
};

//building a constructor function for the student(additional property "course")
const Student = function(firstName, birthYear, course){
    //Option 1
    //this.firstName=firstName;
    //this.birthYear=birthYear;
    
    //Option 2
    Person.call(this,firstName,birthYear)
    
    this.course=course;
}

//Linking prototypes
Student.prototype = Object.create(Person.prototype);
//Student.prototype = Person.prototype; //This does not work 

//create a method "introduce"
Student.prototype.introduce = function(){
    console.log(`My name is ${this.firstName} and I study ${this.course}`);
}

const mike = new Student('Mike',2020,'Computer Science');
console.log(mike);//Student {firstName: 'Mike', birthYear: 2020, course: 'Computer Science'}
mike.introduce();//My name is Mike and I study Computer Science
mike.calcAge();//3 (if you don't link, this won't work)

console.log(mike.__proto__);//Person {introduce: ƒ}
console.log(mike.__proto__.__proto__);//{calcAge: ƒ, constructor: ƒ}

console.dir(Student.prototype.constructor)//ƒ Person(firstName, birthYear)
console.log(mike instanceof Student);//true
console.log(mike instanceof Person);//true
console.log(mike instanceof Object);//true

//Change from Person to Student
Student.prototype.constructor=Student;
console.dir(Student.prototype.constructor)//ƒ Student(firstName, birthYear, course)

```

- Car Exercise 3: Using Constructor Function
```js
//Exercise: Using Constructor Function

const Car = function(make, speed){
    this.make=make;
    this.speed=speed;
}

Car.prototype.accelerate = function(){
    this.speed += 10;
    console.log(`${this.make} is going at ${this.speed}km/h`);
}

Car.prototype.brake = function(){
    this.speed -= 5;
    console.log(`${this.make} is going at ${this.speed}km/h`);
}

//1.Use a constructor function to implement an Electric Car(called EV) as a CHILD "class" of Car.
//Besides a make and current speed, the EV also has the current battery charge in % ('charge' peroperty)
const EV = function(make,speed,charge){
    Car.call(this,make,speed);
    this.charge=charge;
}

//Link the prototypes
EV.prototype = Object.create(Car.prototype);

//2. Implement a 'chargeBattery' method which takes an argument 'chargeTo' and sets the battery charge to 'ChargeTo'
EV.prototype.chargeBattery = function(chargeTo){
    this.charge = chargeTo;
}

//3. Implement an 'accelerate' method that will increase the car's speed by 20, and decrease the charge by 1%. 
//Then log a message like this: `Tesla going at 140 km/h, with a charge of 22%`;
//this acclerate will overwrite the one in Car prototype
EV.prototype.accelerate = function(){
    this.speed += 20;
    this.charge--;
    console.log(`Tesla going at ${this.speed} km/h, with a charge of ${this.charge}%`);
}

//4. Create an electric car object and experiment with calling 'accelerate', 'brake', and 'chargeBattery'
const tesla = new EV('Tesla',120,23);
tesla.chargeBattery(90);
console.log(tesla);//EV {make: 'Tesla', speed: 120, charge: 90}
tesla.brake();//Tesla is going at 115km/h
tesla.accelerate();//Tesla going at 135 km/h, with a charge of 89%

```

<br>


### Inheritance Between "Classes": ES6 Classes
```js
class PersonCL{
    constructor(fullName, birthYear){
        this.fullName=fullName;
        this.birthYear=birthYear;
    }

    //Instance methods
    calcAge(){
        console.log(2023-this.birthYear);
    }
    greet(){
        console.log(`Hey ${this.fullName}`);
    }
    get age(){
        return 2023 - this.birthYear;
    }
    set fullName(name){
        if(name.includes(' ')) this._fullName = name;
        else alert(`${name} is not a full name`);
    }
    get fullName(){
        return this._fullName;
    }

    //Static method
    static hey(){
        console.log(`Hey there`);
    }
}

//Inheritance Classes
class StudnetCL extends PersonCL{
    constructor(fullName,birthYear,course){
        //Always needs to happen first
        super(fullName,birthYear);
        this.course=course;
    }

    //adding introduce method
    introduce(){
        console.log(`My name is ${this.fullName} and I study ${this.course}`);
    }

    //overwriting parent's method "clacAge"
    calcAge(){
        console.log(`I'm ${2023-this.birthYear} years old`)
    }
}

const martha = new StudnetCL('Marta Jones', 2000, 'Computer Science');
console.log(martha);
martha.introduce();//My name is Marta Jones and I study Computer Science
martha.calcAge();//I'm 23 years old
```

<br>

### Inheritance Between "Classes": Object.create

```js
const PersonProto = {
    calcAge(){
        console.log(2023 - this.birthYear);
    },
    init(firstName, birthYear){
        this.firstName=firstName;
        this.birthYear=birthYear;
    },
};

const steven = Object.create(PersonProto);
console.log(steven);//{}

//Linking
const StudentProto = Object.create(PersonProto);

StudentProto.init = function(firstName, birthYear,course){
    PersonProto.init.call(this, firstName,birthYear);
    this.course = course;
}

StudentProto.introduce = function(){
    console.log(`My name is ${this.firstName} and I study ${this.course}`);
}

const jay = Object.create(StudentProto);
jay.init('Jay',1992,'Computer Science');

jay.introduce();//My name is Jay and I study Computer Science
jay.calcAge();//31
```

<br>

### Another Class Example

```js
class Account {
    constructor(owner, currency, pin){
        this.owner=owner;
        this.currency=currency;
        this.pin=pin;
        this.movements=[];
        this.locale=navigator.language;
        console.log(`Thanks for opening an account, ${owner}`);
    }

    //Public Interface (API)
    deposit(val){
        this.movements.push(val);
    }

    withdraw(val){
        this.deposit(-val);
    }

    approveLoan(val){
        return true
    }
    requestLoan(val){
        if(this.approveLoan(val)){
            this.deposit(val);
            console.log('Loan approved!');
        }
    }
}

const acc1 = new Account('Jonas','EUR',1111);//Thanks for opening an account, Jonas
console.log(acc1);//Account {owner: 'Jonas', currency: 'EUR', pin: 1111, movements: [], locale: 'en-US'}

//1. pushing manually (Not a good practice)
acc1.movements.push(100);
console.log(acc1);;//Account {owner: 'Jonas', currency: 'EUR', pin: 1111, movements: [100], locale: 'en-US'}

//2. Instead, Using Public Interface instead of acc1.movements.push(100);
acc1.deposit(250);
acc1.withdraw(140);
console.log(acc1);//Account {owner: 'Jonas', currency: 'EUR', pin: 1111, movements: [100,250,-140], locale: 'en-US'}

acc1.requestLoan(1000);//Loan approved!

//shouldn't be able to call this approveLoan from outside => Need Data privacy/Encapsulation
acc1.approveLoan(1000);
```

<br>

### Encapsulation: Protected Properties and Methods

- Adding underscore convetion: Protected Property => should be only accessable internally

```js

class Account {
    constructor(owner, currency, pin){
        this.owner=owner;
        this.currency=currency;
        //Protected Property
        this._pin=pin;
        //Protected Property (_)
        this._movements=[];
        this.locale=navigator.language;
        console.log(`Thanks for opening an account, ${owner}`);
    }

    //Public Interface (API)
    getMovements(){
        return this._movements
    }

    deposit(val){
        this._movements.push(val);
    }

    withdraw(val){
        this.deposit(-val);
    }

    _approveLoan(val){
        return true
    }

    requestLoan(val){
        if(this.approveLoan(val)){
            this.deposit(val);
            console.log('Loan approved!');
        }
    }
}
```

<br>

### Encapsulation: Protected Class Fields and Methods
- Public fields
- Private fields
- Public methods
- Private methods

```js
class Account {
    //1. Public fields(instances)
    //how to define public fields => no need const &  need semicolon at the end
    //these properties will be presented to all the instances
   locale = navigator.language;
    
    //2. Private fields
    // add # in front
    #movements = [];
    #pin;

    constructor(owner, currency, pin){
        this.owner=owner;
        this.currency=currency;
        //Redefining private filed
        this.#pin=pin;
        console.log(`Thanks for opening an account, ${owner}`);
    }

    //3. Public methods
    //Public Interface (API)
    getMovements(){
        return this.#movements
    }

    deposit(val){
        this.#movements.push(val);
    }

    withdraw(val){
        this.deposit(-val);
    }

    requestLoan(val){
        if(this.#approveLoan(val)){
            this.deposit(val);
            console.log('Loan approved!');
        }
    }

    //4. Private methods => this will work in the future
    #approveLoan(val){
        return true
    }
}

const acc1 = new Account('Jonas','EUR',1111);//Thanks for opening an account, Jonas

//console.log(acc1.#movements);//Private field '#movements' must be declared in an enclosing class 
//console.log(acc1.#pin);//Private field '#pin' must be declared in an enclosing class 
console.log(acc1.getMovements());//still works
```

<br>

### Chaining Methods
- adding ```return this;``` will make it chainable
    
    ```js
    class Account {
        //Public fields(instances)
    locale = navigator.language;
        
        //Private fields
        #movements = [];
        #pin;

        constructor(owner, currency, pin){
            this.owner=owner;
            this.currency=currency;
            this.#pin=pin;
            console.log(`Thanks for opening an account, ${owner}`);
        }

        getMovements(){
            return this.#movements
        }

        deposit(val){
            this.#movements.push(val);
            //adding return this to make it chainable
            return this;
        }

        withdraw(val){
            this.deposit(-val);
            //adding return this to make it chainable
            return this;
        }

        requestLoan(val){
            if(this._approveLoan(val)){
                this.deposit(val);
                console.log('Loan approved!');
            }
            //adding return this to make it chainable
            return this;
        }

        _approveLoan(val){
            return true
        }
    }

    const acc1 = new Account('Jonas','EUR',1111);//Thanks for opening an account, Jonas

    //Chaining -=> adding "return this"" in the method will make it chainable
    acc1.deposit(300).deposit(500).withdraw(35).requestLoan(2500).withdraw(4000)//Loan approved!
    console.log(acc1.getMovements());//[300, 500, -35, 2500, -4000]
    ```

- Car Exercise 4: Using Child Class
  ```js
    // Re-create using ES6 Classes

    class CarCl {
        constructor (make, speed){
            this.make = make;
            this.speed = speed;
        }

        accelerate(){
            this.speed += 10;
            console.log(`${this.make} is going at ${this.speed}km/h`);
            return this;
        }

        brake(){
            this.speed = this.speed - 5;
            console.log(`${this.make} is going at ${this.speed}km/h`);
        }
        
        get speedUS(){
            return this.speed / 1.6;
        }

        set speedUS(speed){
            this.speed = speed * 1.6;
        }
    }

    //Convert this to Child class of Car class
    /*
    const EV = function(make, speed, charge){
        CarCl.call(this, mkae, speed);
        this.charge = charge;
    }

    //Link the prototypes
    EV.prototype = Object.create(Car.prototype);
    EV.prototype.chargeBattery = function(chargeTo){
        this.charge = chargeTo;
    }

    EV.prototype.accelerate = function(){
        this.speed +=20;
        this.charge--;
        console.log(`${this.make} is going at ${this.speed} km/h, with a charge of ${this.charge}`);
    }
    */

    // 1. Create an 'EVCl' child class of teh 'CarCl' class
    class EVCl extends CarCl {
        // 2. Make the 'charge' property private
        #charge;

        constructor(make, speed, charge){
            super(make, speed);
            //3. Redefining private field
            this.#charge = charge;
        }

        chargeBattery(chargeTo){
            this.#charge = chargeTo;
            //4. adding return this to be chainable
            return this;
        }

        accelerate(){
            this.speed +=20;
            this.#charge--;
            console.log(`${this.make} is going at ${this.speed} km/h, with a charge of ${this.#charge}`);
            //4. adding return this to be chainable
            return this;
        }
    }

    const car1 = new EVCl('Rivian', 120, 23);
    console.log(car1);//EVCl {make: 'Rivian', speed: 120, #charge: 23}
    car1.accelerate().accelerate().chargeBattery(30);
    //Rivian is going at 140 km/h, with a charge of 22
    //Rivian is going at 160 km/h, with a charge of 21
    console.log(car1);//EVCl {make: 'Rivian', speed: 160, #charge: 30}
    //console.log(car1.#charge);//Private field '#charge' must be declared in an enclosing class

  ```

<br>

### ES6 Classes Summary

```js
//1. Studnet: Child class
//2. Person: Parent class
//3. extends: Inheritance between classes, automatically sets prototype
class Student extends Person{
    //4. Public field(similar to property, available on created object)
    university = 'University of Lisbon';
    //5. Private fields(not accessible outside of class)
    #studyHours = 0;
    #course;
    //6. Static public field(available only on class)
    static numSubjects = 10;

    //7. Constructor method, called by new operator. Mandatory in regular class, might be omitted in a child class
    constructor(fullName, birthYear, startYear, course){
        //8. Call to parent(super) class(necessary with extend). Needs to happen before accessing this
        super(fullName, birthYear);
        //9. Instance property(available on created ojbect)
        this.startYear = startYear;
        //10. Redefining private field
        this.#course = course;
    }

    //11. Public method
    introduc(){
        console.log(`I study ${this.#course} at ${this.university}`);
    }

    study(h){
        //12. Referencing private field and method
        this.#makeCoffee();
        this.#studyHours +=h;
    }

    //13. Private method(Might not yet work in your browser. "Fake" alternative: _ instead of #)
    #makeCoffee(){
        return 'Here is a coffee for you';
    }

    //14. Getter method
    get testScore(){
        return this._testScore;
    }

    //15.Setter method(use _ to set property with same name as method, and also add getter)
    set testScore(score){
        this._testScore = score <= 20 ? score: 0;
    }

    //16. Static method(available only on class. Can not access instance properties nor methods, only static ones)
    static printCurriculum(){
        console.log(`There are ${this.numSubjects} subjects` );
    }
}

//17. Creating new object with new operator
const studnet = new Studnet('Jonas', 1992, 2023, 'Medicine');

//18. Classes are just "syntactic sugar" over constructor functions
//19. Classes are not hoisted
//20. Classes are first-class citizens
//21. Class body is always executed in strict mode
```