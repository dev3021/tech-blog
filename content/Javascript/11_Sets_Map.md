+++
title = "Sets & Maps"
weight = 11
draft = false
+++


### Set 
- collection of unique value, cannot have any duplicates
- can hold the mixed data type
- there is no index
- Set.size (array: length)
- Set.has (array: includes)
- Set.add / Set.delete
- Example 1
    ```js
    console.log(new Set(`Kelly`)); //{'K', 'e', 'l', 'y'}
    console.log(new Set(`Kelly`).size);//4

    const ordersSet = new Set([`Pasta`,`Pizza`,`Pizza`,`Risotto`,`Pasta`,`Pizza`]);

    console.log(ordersSet);//{'Pasta', 'Pizza', 'Risotto'}
    console.log(ordersSet.size);//3
    console.log(ordersSet.has(`Pizza`));//true
    console.log(ordersSet.has(`Bread`));//false

    //adding new element
    ordersSet.add(`Garlic Bread`);
    console.log(ordersSet);//{'Pasta', 'Pizza', 'Risotto', 'Garlic Bread'}

    //delete element
    ordersSet.delete(`Risotto`);
    console.log(ordersSet);//{'Pasta', 'Pizza', 'Garlic Bread'}

    //no index
    console.log(ordersSet[0]);//undefined (there is no index)

    for(const order of ordersSet){
    console.log(order);
    }

    //delete all element
    ordersSet.clear();
    console.log(ordersSet);//{size: 0}
    ```
- Example2
  ```js
    const staff = [`Waiter`,`Chef`,`Waiter`,`Manager`,`Chef`,`Waiter`];

    //delete the duplicate => create Set
    const staffUnique = new Set(staff);
    console.log(staffUnique);//{'Waiter', 'Chef', 'Manager'}
    console.log(staffUnique[0]);//undefined

    //change to Array
    const staffUniqueArray = [...new Set(staff)];
    console.log(staffUniqueArray);//['Waiter', 'Chef', 'Manager']
    console.log(staffUniqueArray[0]);//Waiter
  ```
  <br>


### Maps
- difference between Object and map is that in maps, the keys can have any type. in objects, the keys are always strings
- Map.set(set: add) / Map.get / Map.delete / Map.size / Map.has
  ```js
    //create an empty map
    const rest = new Map();

    //to add new element: set
    rest.set('name','Classico Italiano');
    rest.set(1,'Firenze, Italy');
    rest.set(2,`Lisbon, Portugal`);

    //set method returns the map
    console.log(rest.set(2,`Lisbon, Portugal`));//{'name' => 'Classico Italiano', 1 => 'Firenze, Italy', 2 => 'Lisbon, Portugal'}
    console.log(rest);//{'name' => 'Classico Italiano', 1 => 'Firenze, Italy', 2 => 'Lisbon, Portugal'}

    rest.set('catagories',[`Italian`,'Pizzeria','Vegetrian','Organic']) //returning updated map
        .set('open',11) //returning updated map
        .set('close',23) //returning updated map
        .set(true,'We are open')
        .set(false,'We are closed');

    console.log(rest);
    /*{ 'name' => 'Classico Italiano', 
        1 => 'Firenze, Italy', 
        2 => 'Lisbon, Portugal', 
        'catagories' => Array(4), 
        'open' => 11,
        'close' => 23,
        true => 'We are open',
        false => 'We are closed
    }
    */

    //get method
    console.log(rest.get('name'));//Classico Italiano
    console.log(rest.get(true));//We are open
    console.log(rest.get(1));//Firenze, Italy
    console.log(rest.get('1'));//undefined

    const time = 21;
    console.log(rest.get(time > rest.get('open') && time < rest.get('close')));//We are open (true)

    //has method
    console.log(rest.has('catagories'));//true

    //delete element
    rest.delete(2);

    //size
    console.log(rest.size);//7

    //remove all the element
    rest.clear();
    console.log(rest);//{size: 0}

    rest.set([1,2],`Test`);
    console.log(rest.get([1,2]));//undefined (it's not the same object in heap)

    //to make this work
    const arr = [1,2];
    rest.set(arr,'Test');
    console.log(rest.get(arr));//Test

    rest.set(document.querySelector('h1'),`Heading`);
    console.log(rest);//{h1 => "Heading"}
    ```

### Maps: Iteration
- Exampe 1: Convert Object to Map
  ```js
    const openingHours= {
    thur:{
        open:12,
        close:22
    },
    fri:{
        open:11,
        close:23
    },
    sat:{
        open: 0,
        close:24
    }
    };

    console.log(Object.entries(openingHours));
    /*
    0: Array(2)
    0:"thur"
    1: {open: 12, close: 22}
    1: Array(2)
    0:"fri"
    1: {open: 11, close: 23}
    2: Array(2)
    0:"sat"
    1: {open: 0, close: 24}
    */

    //Convert Object to Map
    const hoursMap = new Map(Object.entries(openingHours));
    console.log(hoursMap);
    /*
    0: {"thur" => Object}
    key: "thur"
    value: {open: 12, close: 22}
    1: {"fri" => Object}
    key: "fri"
    value: {open: 11, close: 23}
    2: {"sat" => Object}
    key: "sat"
    value: {open: 0, close: 24}
    */
    ```
- Example 2:
  ```js
  const question = new Map([
    ['question','What is the best programming language?'],
    [1,'C'],
    [2,'Java'],
    [3,'JavaScript'],
    ['correct',3],
    [true, 'Correct!'],
    [false, 'Try again!']
    ])

    //Quiz App
    console.log(question.get('question'));//What is the best programming language?

    for(const [key,value] of question){
    if(typeof key === 'number'){
        console.log(`Answer ${key}: ${value}`);
    }
    }

    const answer = Number(prompt('Your answer'));
    console.log(`Your answer is ${answer}: ${question.get(answer)}`);//Your answer is 3: JavaScript

    // console.log(question.get('correct'));//3
    // console.log(question.get(3));//JavaScript
    // console.log(question.get('correct')===answer);//true
    console.log(question.get(question.get('correct')===answer)) //Correct!

    //convert Map to Array
    console.log([...question]);
    console.log(question.entries());
    console.log(...question.keys());
    console.log(...question.values());
    ```

<br>

### Summary: Which Data Structure to Use?

#### Arrays VS Sets
- Arrays
  ```js
  tasks = ['Code','Eat','Code']; //['Code','Eat','Code']
  ```
  - use when you need ordered list of values(might contain duplicates)
  - use when you need to manipulate data

- Sets
  ```js
  tasks = new Set(['Code','Eat','Code']); //['Code','Eat']
  ```
  - use when you need to work with unique values
  - use when high-performance is really important
  - use when to remove duplicates from arrays



#### Objects VS Maps
- Objects:
  ```js
  tasks = {
    tasks: 'Code',
    date: 'today',
    repeat: true  
  };
  ```
  - more traditional key/value stores("aubsed objects)
  - easier to writ and access values with . and []
  - use when you need to include functions(methods)
  - use when working with JSON(can convert to map)
  
- Maps:
  ```js
  tasks = new Map([
    ['task','Code'],
    ['date','today'],
    [false,'Start coding']
  ]);
  ```
  - better performance
  - keys can have any data type
  - easy to iterate
  - easy to compute size
  - use when you simply need to map key to values
  - use when you need keys that are not strings
