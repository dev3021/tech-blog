+++
title = "3 Types of Scope"
weight = 5
draft = false
+++


### 3 Types of Scope
- Global Scope
  - outside of any function or block
  - variables declared in global scope are accessbile everywhere
    ```js
      const me = "Kelly";
      const job = "Engineer";
    ```
- Function Scope
  - Variables are accessible only inside function, NOT outside
  - also called local scope
    ```js
    function calcAge(birthYear){
      const now = 2023;
      const age = now - birthYear;
      return age;
    }

    console.log(now);//ReferenceError
    ```
- Block Scope(ES6)
  - Variables are accessible only inside block
  - However, this only applies to **let** and **const** variables
  - **Functions** are also block scoped(only in strict mode)
    ```js
    if(year >= 1991 && year <= 1996){
      const millenial = true;
      const food = 'Toast';
    }
    console.log(millenial);//ReferenceError => outside of block
    ```

<br>

### Hoisting and TDZ(Temporal Dead Zone)
- **Hoisting**: Makes some types of variables accessible/usable in the code before they are actually declared. "Variables lifted to the top of their scope"
  - bind the scenes => Before execution, code is scanned for variable declarations, and for each variables, a new property is created in the variable environment object
- **var**: Hoisted
- **let, const** : **Not Hoisted**
- function declarations: Hoisted
- function expressions and arrows: Depends if using var or let/const
  ```js
  //Variables
  console.log(me); //undefined
  console.log(job); //Uncaught ReferenceError: Cannot access 'job' before initialization
  console.log(year); //Uncaught ReferenceError: Cannot access 'year' before initialization

  var me = "Kelly";
  let job = "Engineer";
  const year = 1992;

  console.log(addDecl(2,3)); //5 => addDecl is function declarations(hoisted), so you can call it
  console.log(addExpr(2,3)); //Uncaught ReferenceError: Cannot access 'addExpr' before initialization
  console.log(addArrow(2,3));//Uncaught TypeError: addArrow is not a function

  //function declarations
  function addDecl(a,b){
      return a+b;
  }
  //function expressions
  const addExpr = function(a,b){
      return a+b;
  }
  //function arrows
  var addArrow = (a,b) => a+b;
  ```

