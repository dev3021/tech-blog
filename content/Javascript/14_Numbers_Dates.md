+++
title = "Numbers and Dates"
weight = 14
draft = false
+++

### Converting and Checking Numbers

```js
console.log(23 === 23.0); //true
console.log(0.1+0.2); //0.30000000000000004
console.log( 0.1+0.2 === 0.3);//false

//Conversion
//You can use + instead of Number()
console.log(Number('23'));//23
console.log(+'23');//23

//Parsing
console.log(Number.parseInt('30px'));//30
console.log(Number.parseInt('30px',10));//30
console.log(Number.parseInt('e23'));//NaN

console.log(Number.parseFloat('2.5rem'));//2.5
console.log(Number.parseInt('2.5rem'));//2
console.log(parseInt('2.5rem'));//2

//Check if value is NaN
console.log(Number.isNaN(20));//false
console.log(Number.isNaN('20'));//false
console.log(Number.isNaN(+'20X'));//true
console.log(Number.isNaN(23/0));//false (Infinity)

//Best wayt to check if value is number or not
console.log(Number.isFinite(20));//true
console.log(Number.isFinite('20'));//false
console.log(Number.isFinite(+'20X'));//false (NaN)

console.log(Number.isInteger(23));//true
console.log(Number.isInteger(23.0));//true
```


### Math and Rounding

```js
console.log(Math.sqrt(25));//5
console.log(25 ** (1/2));//5
console.log(8 ** (1/3));//2

console.log(Math.max(5,18,23,11,2));//23
console.log(Math.max(5,18,'23',11,2));//23
console.log(Math.max(5,18,'23px',11,2));//NaN

console.log(Math.min(5,18,23,11,2));//2

console.log(Math.PI);//3.141592...

console.log(Math.PI * Number.parseFloat('10px')**2);//314.1592...

console.log(Math.trunc(Math.random() * 6)+1);

//random number between min and max
// 0...1 -> 0...(max-min) -> min...(max-min+min) -> min...max
const randomInt = (min, max) => Math.floor(Math.random()*(max-min)+1)+min;
//random number between 10 and 20
console.log(randomInt(10,20));

//Rounding integers
console.log(Math.trunc(23.3));//23

//round: nearest integer
console.log(Math.round(23.3));//23
console.log(Math.round(23.9));//24

//ceil: round up
console.log(Math.ceil(23.3));//24
console.log(Math.ceil(23.9));//24

//floor: round down
console.log(Math.floor(23.3));//23
console.log(Math.floor(23.9));//23

//floor and trunc is the same for positive number, not negative number
console.log(Math.trunc(-23.3));//-23
console.log(Math.floor(-23.3));//-24

//Rounding decimals
//toFixed will return String not a Number
console.log((2.7).toFixed(0));//3 (string)
console.log((2.7).toFixed(3));// 2.700 (string)
console.log((2.345).toFixed(2));// 2.35 (string)
console.log(+(2.345).toFixed(2));//2.35 (number) //+ will convert String to Number

```

### The Remainder Operator

```js
console.log(5 % 2);//1
console.log(8 % 3);//2

//check number is even or odd
const insEven = n => n % 2 === 0;
console.log(insEven(8));//true
console.log(insEven(23));//false

```

### Numberic Separators

```js
//287,460,000,000
const diameter = 287_460_000_000; //use underscore to read easily
console.log(diameter);//287460000000

const price = 345_99
console.log(price);//34599

const transferFee = 15_00
console.log(transferFee);//1500

console.log(Number('230000'));//230000
console.log(Number('230_000'));//NaN

```

### Working with BigInt
```js
//biggest number that is safe in Javascript 
console.log(2 ** 53 - 1);//9007199254740991
console.log(Number.MAX_SAFE_INTEGER);//9007199254740991
console.log(2 ** 53 + 3);//9007199254740996 => Not correct

console.log(4524524534534524523234234234234);//4.5245245345345243e+30
//adding n at the end => conver to BigInt
console.log(4524524534534524523234234234234n);//4524524534534524523234234234234n

console.log(BigInt(234542524));//234542524n

//Operations
console.log(10000n + 10000n);//20000n

//cannot mix BigInt with regular number
const huge = 234354643634634n;
const num =23;
console.log(huge * num);//script.js:288 Uncaught TypeError: Cannot mix BigInt and other types, use explicit conversions
console.log(huge * BigInt(num));//5390156803596582n

console.log(20n > 15);//true
console.log(20n === 20)//false
console.log(20n == 20);//true
console.log(typeof 20n);//bigint

//Divisions
console.log(10n/3n);//3n
console.log(10/3);//3.3333333333333335
```

### Creating Dates
```js
//Create a date
const now = new Date();
console.log(now);//Thu Jun 22 2023 08:50:58 GMT-0400 (Eastern Daylight Time)
console.log(new Date('December 24, 2023'))//Sun Dec 24 2023 00:00:00 GMT-0500 (Eastern Standard Time)

const future = new Date(2037, 10, 19, 15, 23);
//10 + 1 => 11 => November
console.log(future);//Thu Nov 19 2037 15:23:00 GMT-0500 (Eastern Standard Time)
console.log(future.getFullYear());//2037
console.log(future.getMonth());//10
console.log(future.getDate());//19
console.log(future.getDay());//4 (0 = Sunday, 4 = Thurs)
console.log(future.getHours());//15
console.log(future.getMinutes());//23
console.log(future.getSeconds());//0
console.log(future.toISOString());//2037-11-19T20:23:00.000Z
console.log(future.getTime());//2142274980000
console.log(new Date(2142274980000));//Thu Nov 19 2037 15:23:00 GMT-0500 (Eastern Standard Time)

//timestamp
console.log(Date.now());//1687438479588
console.log(new Date(1687438479588));//Thu Jun 22 2023 08:54:39 GMT-0400 (Eastern Daylight Time)

future.setFullYear(2040);
console.log(future);//Mon Nov 19 2040 15:23:00 GMT-0500 (Eastern Standard Time)

```

### Operations With Dates

```js
const future = new Date(2037, 10, 19, 15, 23);
console.log(future);//Thu Nov 19 2037 15:23:00 GMT-0500 (Eastern Standard Time)
console.log(+future);//2142274980000

//calculate days difference
const calcDaysPassed = (date1, date2) => Math.abs(date2 - date1) / (1000 * 60 * 60 * 24);
const days1= calcDaysPassed(new Date(2037,3,14), new Date(2037,3,4));
console.log(days1);//10
```

### Internationalizing Dates (Intl)
- [ISO Language Code Table](http://www.lingoes.net/en/translator/langcode.htm)
  
```js
//Using API
const now = new Date();

console.log(Intl.DateTimeFormat('en-US').format(now));// 6/23/2023
console.log(Intl.DateTimeFormat('en-GB').format(now));// 23/06/2023

const options1 = {
  hour: 'numeric',
  minute: 'numeric'
}

console.log(Intl.DateTimeFormat('en-US',options1).format(now));// 9:53 AM

const options2 = {
  hour: 'numeric',
  minute: 'numeric',
  day:'numeric',
  month:'numeric',
  year:'numeric',
  weekday: 'long'
}

console.log(Intl.DateTimeFormat('en-US',options2).format(now));// Friday, 6/23/2023, 9:53 AM

//if month is long => June
//if year is 2-digit => 23
const options3 = {
  hour: 'numeric',
  minute: 'numeric',
  day:'numeric',
  month:'long',
  year:'2-digit',
  weekday: 'short'
}

console.log(Intl.DateTimeFormat('en-US',options3).format(now));// Fri, June 23, 23 at 9:53 AM

//you can get their computer setting 
const locale = navigator.language;
console.log(locale);//en-US
console.log(Intl.DateTimeFormat(locale,options3).format(now));//Fri, June 23, 23 at 9:53 AM
```

### Internationalizing Numbers (Intl)
```js
const num = 38884764.23;

console.log('US: ', new Intl.NumberFormat('en-US').format(num));//US:  38,884,764.23
console.log('Germay: ', new Intl.NumberFormat('de-DE').format(num));//Germay:  38.884.764,23
console.log(navigator.language, new Intl.NumberFormat(navigator.language).format(num));//en-US 38,884,764.23

const options1 = {
  style: "unit", 
  unit: 'mile-per-hour',

}
console.log('US: ', new Intl.NumberFormat('en-US',options1).format(num));//US:  38,884,764.23 mph
console.log('Germay: ', new Intl.NumberFormat('de-DE',options1).format(num));//Germay:  38.884.764,23 mi/h


const options2 = {
  style: "unit", 
  unit: 'celsius',
}
console.log('US: ', new Intl.NumberFormat('en-US',options2).format(num));//US:  38,884,764.23°C
console.log('Germay: ', new Intl.NumberFormat('de-DE',options2).format(num));//Germay:  38.884.764,23 °C


const options3 = {
  style: "percent", 
}
console.log('US: ', new Intl.NumberFormat('en-US',options3).format(num));//US:  3,888,476,423%
console.log('Germay: ', new Intl.NumberFormat('de-DE',options3).format(num));//Germay:  3.888.476.423 %


const options4 = {
  style: "currency", 
  currency: "EUR" //you have to define currency
}
console.log('US: ', new Intl.NumberFormat('en-US',options4).format(num));//US:  €38,884,764.23
console.log('Germay: ', new Intl.NumberFormat('de-DE',options4).format(num));//Germay:  38.884.764,23 €
```

### Timers: setTimeout and setInterval
- setTimeout timer: runs just once after a defined time
- setInterval timer: keeps running forever until we stop it
- clearTimeout: delete the timer
  
```js
//callback function
//want to call 3 secs later
//as soon as JavaScript hits this line of code here, it will simply keep counting the time in the background, and register this callback function to be called after that time has elasped
//this mechanism is called Asynchronous JavaScript
setTimeout(() => console.log('Here is your pizza'), 3000); //Here is your pizza (3 secs later)

setTimeout((ing1, ing2) => console.log(`Here is your pizza with ${ing1} and ${ing2}`), 3000, 'olives', 'spinach'); //Here is your pizza (3 secs later)

//using clearTimeout => to delete the timer
const ingredients = ['olives','spinach'];
//using spread operator
const pizzaTimer = setTimeout((ing1, ing2) => console.log(`Here is your pizza with ${ing1} and ${ing2}`), 5000, ...ingredients);
//if ingredients include spinach, it will delete the timer
if(ingredients.includes('spinach')) clearTimeout(pizzaTimer);

```

```js
//setInterval
//every 1 sec, display the current time
setInterval(function(){
    const now = new Date();
    console.log(now);
},1000);
```
