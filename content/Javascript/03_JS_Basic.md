+++
title = "Javascript Basic"
weight = 3
draft = false
+++
<!--when the draft = true => it wont' show on the website 
weight will decide the order of menu (the lower the first )-->


##### Variable naming convention:
- Standard: **camelCase**
  - ```let firstNamePerson = "Kelly";```
- illegal
  - ``` let 3years =3 ;```: SyntaxError
  - ``` let jonas&matilda = "JM";```: SyntaxError
  - ``` let new = 27; ```: SyntaxError


##### The 7 Primitive Data Types

1. **Number**: Floating point numbers: Used for decimals and integers ```let age = 23;```

2. **String**: Sequence of characters: Used for text ```let firstName = 'Kelly';```

3. **Boolean**: ```let fullAge = true;```

4. **Undefined**: Value taken by a variable that is not yet defined(empty value) 
    ```js
    let year;
    console.log(year); // undefined
    console.log(typeof year); // undefined
    year = 2023;
    console.log(year); // 2023
    console.log(typeof year); // number
    ```
5. **Null**: also means emtpy value
6. Symbol (ES2015): Value that is unique and cannot be changed (not useful for now)
7. BigInt(ES2020): Larger integers than the Number type can hold

##### Dynamic Typing
- JavaScript has **dynamic typing**: we do not have to manaully define the data type of the value stored in a variable. Instead, data types are determined automatically
    ```js
    let javascriptIsFun = true;
    console.log(typeof javascriptIsFun); // boolean
    javascriptIsFun = "Kelly";
    console.log(typeof javascriptIsFun); // string
    ```

##### typeof

    ```js
    console.log(typeof true);//boolean
    console.log(typeof 23);//number
    console.log(typeof 'Kelly');//string
    ```

##### let, const and var 
- **let**: mutable(You can change values), block scoped
- **const**: immutable(You cannnot change values after you define)
    ```js
    const birthYear = 1991;
    birthYear = 1990; // TypeError
    const job; // cannot use undefined
    ```
- var: should be completely avoided (old way of defining variable prior to ES6), function scoped


##### Basic Operator
- Math Operators
- Assignment Operators
    ```js
    let x = 10+5; //15
    x += 10; //x=x+10=25
    x *= 4; //x=x*4=100
    x++; //x=x+1=101
    x--; //x=x-1=100
    ```   
- Comparion Operators


##### Strings and Template Literals
- using **backticks** 
    ```js
    const firstName = 'Kelly';
    const kellyNews = `I'm ${firstName}`;
    console.log(kellyNews) // I'm Kelly
    
    // you can use backticks for String as well
    console.log(`I'm Kelly`)

    console.log('String with\n\multiple\n\lines');

    // if you use the backticks, same results
    console.log(`String with
    multiple
    line`);
    ```

##### Type conversion and coercion
- Type Conversion: convert String to Number: **Number()**
    ```js
    const inputYear = '1991';
    console.log(inputYear + 18); //199118
    console.log(Number(inputYear)); //1991
    console.log(Number(inputYear) + 18); //2009
    console.log(Number('Kelly')); // NaN (Not A Number)
    ```
- Type Conversion: convert Number to String: **String()**
    ```js
    console.log(String(23));
    ```
- Type Coercion(**Automatic**)
    ```js
    console.log('I am ' + 23 + ' years old');
    console.log('23'-'10'-3); //10
    console.log('10'-'4'-'3'-2+'5'); // String "15"
    ```

##### Equality Operators
- strict equality operator **===**
- loose equality operator **==**
    ```js
    console.log('18'===18);//false
    console.log(18===18);//true
    console.log('18'==18);//true
    ```

##### Prompt
```js
const favorite = prompt("What's your favoriate number?");
console.log(favorite);
```


##### The Switch Statement
```js
const day = 'monday';

switch(day){
    case 'monday': // day==='monday'
        console.log('Plan course structure');
        break; //if you don't add break here, it will continue to tuesday
    case 'tuesday':
        console.log('Prepare theory videos');
        break;
    case 'wednesday':
    case 'thursday':
        console.log('Write code examples');
        break;
    case 'friday':
        console.log('Record videos');
        break;
    case 'saturday':
    case 'sunday':
        console.log('Enjoy the weekend');
        break;
    default:
        console.log('Not a valid day');
}
```


##### The Conditional(ternary) Operator
```js
//example1
const age = 23;
age >= 18 ? console.log("wine") : console.log(water)

//example2
const drink = age >= 18 ? 'wine' : 'water';
console.log(drink);

//example3
console.log(`I like to drink ${age >= 18 ? 'wine' : 'water'}`);
```


##### Strict Mode
- ```'use strict';```: in the beginning of the script to make it more secure

##### Functions
```js
function logger(){
    console.log('My name is Kelly');
}

// calling / running / invoking function
logger();

// using parameters
function fruitProcessor(apples, oranges){
    console.log(apples, oranges);
    const juice = `Juice with ${apples} apples and ${oranges} oranges.`;
    return juice;
}

const appleJuice = fruitProcessor(5,0); 
console.log(appleJuice); // Juice with 5 apples and 0 oranges.
```

##### Function Declarations vs Expressions
- Function Declarations: you can call the function before they are defined in the code
- Function Expressions: you have to define function first before calling them
```js
// Function Declarations
function calcAge1(birthYear){
    return 2023 - birthYear;
}
const age1 = calcAge1(1992);
console.log(age1);//31

// Function Expressions
const calcAge2 = function(birthYear){
    return 2023 - birthYear;
}
const age2 = calcAge2(1992);
console.log(age2);
```

##### Arrow Functions
- It was added in ES6
    ```js
    // Arrow function 1
    const calcAge3 = birthYear => 2023 - birthYear;
    const age3 = calcAge3(1992);
    console.log(age3);

    // Arrow Function 2
    const yearsUtilRetirement = birthYear => {
        const age = 2023 - birthYear;
        const retirement = 65 - age;
        return retirement;
    }
    console.log(yearsUtilRetirement(1992));

    // Arrow Function 3 / with parameter
    const yearsUtilRetirement = (birthYear, firstName) => {
        const age = 2023 - birthYear;
        const retirement = 65 - age;
        return `${firstName} retires in ${retirement} years`;
    }
    console.log(yearsUtilRetirement(1992,'Kelly'));

    ```

##### Array
```js
const friends = ['Kelly','Cam','Steve'];
const years = new Array(1991,1992,1993);

// how to replace
// even though it's defined with const, because array is not primitive data, it's mutable.
friends[2] = 'Jay'; 
console.log(firends);//'Kelly','Cam','Jay'

const firstName = 'kelly';
const kelly = [firstName,'Lee',2023-1992, friends]; //["kelly","Lee",31,Array(3)]
```

##### Basic Array Operations
- **push()**: add the element at the end
    ```js
    const friends = ['Kelly','Cam','Steve'];
    friends.push('Jay'); 
    console.log(friends);//['Kelly','Cam','Steve','Jay']
    ```
- **unshfit()**: add the element in the beginning
    ```js
    const friends = ['Kelly','Cam','Steve'];
    friends.unshift('Jay');
    console.log(friends);//['Jay','Kelly','Cam','Steve']
    ```
- **pop()**: remove the last elemet of array
    ```js
    const friends = ['Kelly','Cam','Steve'];
    const popped = friends.pop();
    console.log(popped);//Steve 
    console.log(friends);//['Kelly','Cam']
    ```
- **shift()**: remove first element of array
    ```js
    const friends = ['Kelly','Cam','Steve'];
    const shift = friends.shift();
    console.log(shift);//kelly 
    console.log(friends);//['Cam','Steve']
    ```
- **indexOf()**
    ```js
    const friends = ['Kelly','Cam','Steve'];
    console.log(friends.indexOf('Steve'));//2
    ```
- **includes()**: added in ES6
    ```js
    const friends = ['Kelly','Cam','Steve'];
    console.log(friends.includes('Kelly'));//true
    console.log(friends.includes('Bob'));//false
    friends.push(23);
    console.log(friends);//'Kelly','Cam','Steve',23
    console.log(friends.includes(23));//true
    console.log(friends.includes('23'));//false
    ```

##### Objects
```js
// Joans object has 5 properties
const jonas = {
    firstName: 'Jonas',
    lastName: 'Schedtmann',
    age: 2023-1992,
    job: 'teacher',
    friends: ['Kelly','John']
}
```

##### Dot vs Bracket Notation
```js
const jonas = {
    firstName: 'Jonas',
    lastName: 'Schedtmann',
    age: 2023-1992,
    job: 'teacher',
    friends: ['Kelly','John']
}

// Using Dot
console.log(jonas.lastName); //Schedtmann
// Using Bracket 
console.log(jonas['lastName']);//Schedtmann

// why you need to use bracket(you can't do this with Dot)
const nameKey = 'Name';
console.log(jonas['first'+nameKey]); //Jonas
console.log(jonas['last'+nameKey]); //Schedtmann

// example of using bracket 
const interestedIn = prompt('What do you want to know about Jonas? choose between firstName, lastName, age, job, and friends');

if(jonas[interestedIn]){
    console.log(jonas[interestedIn]);
}else{
    console.log('Wrong request')
}

// adding property using Dot
jonas.location = 'Portugal';
console.log(jonas);

// adding property using bracket
jonas['twiiter']='@jonass';
console.log(jonas);

console.log(`${jonas.firstName} has ${jonas.friends.length} friends, and his best friend is ${jonas.friends[0]}`); // jonas has 2 friends, and his best friend is Kelly
```

##### Object Methods
```js
const jonas = {
    firstName: 'Jonas',
    lastName: 'Schedtmann',
    birthyear: 1992,
    job: 'teacher',
    friends: ['Kelly','John'],
    hasDriverLicense: true,
    // you can add function as an object property
    calcAge: function(birthyear){
        return 2023 - birthyear;
    }
}

// using dot
console.log(jonas.calcAge(1992)); //31
// using bracket
console.log(jonas['calcAge'](1992)); //31
```
```js
// using this.
const jonas = {
    firstName: 'Jonas',
    lastName: 'Schedtmann',
    birthyear: 1992,
    job: 'teacher',
    friends: ['Kelly','John'],
    hasDriverLicense: true,
    // using this keyword
    calcAge: function(){
        console.log(this);
        return 2023 - this.birthyear;
    }
}

// using dot
console.log(jonas.calcAge()); //31
// using bracket
console.log(jonas['calcAge']()); //31
```
```js
const jonas = {
    firstName: 'Jonas',
    lastName: 'Schedtmann',
    birthyear: 1992,
    job: 'teacher',
    friends: ['Kelly','John'],
    // creating new property age and save it
    calcAge: function(){
        this.age = 2023 - this.birthyear;
        return this.age;
    }
}

console.log(jonas.calcAge()); //31
// after you call calcAge once, you can use age
console.log(jonas.age); //31
```

```js
// Challenge
/// "Jonas is a 31-year old teacher, and he has a driver's license"

const jonas = {
    firstName: 'Jonas',
    lastName: 'Schedtmann',
    birthyear: 1992,
    job: 'teacher',
    friends: ['Kelly','John'],
    hasDriverLicense: true,
    calcAge: function(){
        this.age = 2023 - this.birthyear;
        return this.age;
    },
    getSummary: function(){
        return `${this.firstName} is a ${this.calcAge()}-year old ${this.job}, and he has ${this.hasDriverLicense ? 'a' : 'no'} driver's license.`
    }
}

console.log(jonas.getSummary());
/// "Jonas is a 31-year old teacher, and he has a driver's license"
```

##### Iteration: The for Loop
```js
for(let rep = 1; rep <=10; rep++){
    console.log(rep);
}
```

##### Looping Arrays, Breaking and Continuing
```js
// example1
const jonasArray = [
    'jonas',
    23,
    'teacher',
    ['kelly','cam'],
    true
]

const types1 = [];
const types2 = [];

for(let i=0; i<jonasArray.length; i++){
    console.log(jonasArray[i], typeof jonasArray[i])
    // Option1. filing types array
    types1[i] = typeof jonasArray[i];
    // Option2. using push
    types2.push(typeof jonasArray[i]);
}

console.log(types1);//['string','number','string','object','boolean']
console.log(types2);//['string','number','string','object','boolean']


// example2
const years = [1991,2007,1969,2000];
const ages = [];

for(let i=0; i<years.length;i++){
   ages.push(2023-years[i]);
}

console.log(ages); //32,16,54,23

// continue 
for(let i=0; i<jonasArray.length; i++){
    if(typeof jonasArray[i] !== 'string') continue;
    console.log(jonasArray[i], typeof jonasArray[i])
    //jonas string
    //teacher string
}

// break
for(let i=0; i<jonasArray.length; i++){
    if(typeof jonasArray[i] === 'number') break;
    console.log(jonasArray[i], typeof jonasArray[i])
    //jonas string
}

// loop backwards
for(let i=jonasArray.length-1; i>=0; i--){
    console.log(jonasArray[i]);
    
}
```

##### while loop
```js
//using for loop
for(let rep =1; rep <=10; rep++){
    console.log(`Lifting weight repetition ${rep}`);
}

//using while loop
let rep =1;
while(rep<=10){
    console.log(`Lifting weight repetition ${rep}`);
    rep++;
}

```


