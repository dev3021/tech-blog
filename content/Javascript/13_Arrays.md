+++
title = "Arrays"
weight = 13
draft = false
+++

### Simple Array Methods
- slice(): it does not change the original array
- splice(): it changes the original array
  
  ```js
  let arr = ['a','b','c','d','e'];

  //slice(): will return a new array
  console.log(arr.slice(2));//['c', 'd', 'e']

  //length: 4-2 = 2 (4 is not included in the array)
  console.log(arr.slice(2,4));//['c', 'd']
  
  //last two elements
  console.log(arr.slice(-2));//['d', 'e']
  
  //last element
  console.log(arr.slice(-1));//['e']
  
  //from 1 except last 2 elements
  console.log(arr.slice(1,-2));//['b', 'c']
  
  //from 0 except last 1 element
  console.log(arr.slice(0,-1));//['a', 'b', 'c', 'd']
  
  //copy the array
  console.log(arr.slice());//['a', 'b', 'c', 'd', 'e']
  
  //using spread operator
  console.log([...arr]);//['a', 'b', 'c', 'd', 'e']

  //splice(): it does change the original array
  console.log(arr.splice(2));//['c', 'd', 'e']
  console.log(arr);//['a', 'b'] //original array: extracted from splice
  
  //get rid of the last element
  arr.splice(-1);
  console.log(arr);//['a'] //original array: extracted from splice
  ```

- reverse(): it changes the original array

- concat()

- join(): it returns a string
  
  ```js
  //reverse(): it changes the original array
  const arr = ['a','b','c','d','e'];
  const arr2 = ['j','i','h','g','f'];

  console.log(arr2.reverse());//['f', 'g', 'h', 'i', 'j']
  console.log(arr2);//['f', 'g', 'h', 'i', 'j']

  //concat()
  const letters = arr.concat(arr2);
  console.log(letters);//['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
  //another way: spread operator
  console.log([...arr,...arr2]);//['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

  //join(): result is a string
  console.log(letters.join(' - '));//a - b - c - d - e - f - g - h - i - j
  ```

### The new At Method
- at()
  
```js
const arr = [23,11,64];

console.log(arr[0]);//23
console.log(arr.at(0));//23

//three different ways to get the last array element
console.log(arr[arr.length - 1]);//64
console.log(arr.slice(-1)[0]);//64
console.log(arr.at(-1));//64

//at() works in string
console.log('kelly'.at(0));//k
console.log('kelly'.at(-1));//y
```

### Looping Arrays: forEach

```js

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// 1. using for loop
// for(const movement of movements){
// Argument orders: index(i), current element(movement)
for(const [i, movement] of movements.entries()){
  if(movement > 0){
    console.log(`Movement ${i +1}: You deposited ${movement}`);
  }else{
    console.log(`Movement ${i +1}: You withdrew ${Math.abs(movement)}`);
  }
}

// 2. using forEach (require callback function)
// movements.forEach(function(movement){
// Argument orders: current element(movement), index, array 
movements.forEach(function(movement, index, array){
  if(movement > 0){
    console.log(`Movement ${index +1}: You deposited ${movement}`);
  }else{
    console.log(`Movement ${index +1}: You withdrew ${Math.abs(movement)}`);
  }
});
// 0: function(200)
// 1: function(450)...
```


### forEach with Maps and Sets
```js
const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

//Map
currencies.forEach(function(value,key,map){
  console.log(`${key}: ${value}`);
});

//Set
const currenciesUnique = new Set(['USD','GBP','USD','EUR','EUR']);
console.log(currenciesUnique);//{'USD', 'GBP', 'EUR'}

currenciesUnique.forEach(function(value,key,map){
  console.log(`${key}: ${value}`);//USD: USD => there is no key in set, you can use (value, _ , map) instead
})
```

### Data Transformations: map, filter, reduce
- MAP: map returns a new array containing the results of applying an operation on all original array elements
  - ex) [3,1,4,3,2] => map(current * 2) => [6,2,8,6,4]
  
- FILTER: filter returns a new array containing the array elements that passed a specified test condition
  - ex) [3,1,4,3,2] => filter(current > 2) => [3,4,3] (filtered array)

- REDUCE: reduce boils("reduces") all array elements down to one single value(e.g. adding all elements together)
  - ex) [3,1,4,3,2] => reduce(accumulate + current) => 13 (reduced to one single value)


### The Map Method

```js
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
const eruToUsd = 1.1;

//Using Map method
const movementsUSD = movements.map(function(mov){
  return mov * eruToUsd;
});

console.log(movements);//[200, 450, -400, 3000, -650, -130, 70, 1300]
console.log(movementsUSD);//[220.00000000000003, 495.00000000000006, -440.00000000000006, 3300.0000000000005, -715.0000000000001, -143, 77, 1430.0000000000002]
```

### The Filter Method

```js
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

//Using filter method
const deposits = movements.filter(function(mov){
  return mov > 0;
});

//Using Arrow Function
const withdrawals = movements.filter(mov => mov<0);

console.log(movements);//[200, 450, -400, 3000, -650, -130, 70, 1300]
console.log(deposits);//[200, 450, 3000, 70, 1300]
console.log(withdrawals);//[-400, -650, -130]
```

### The Reduce Method
```js
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

//accumulator => SNOWBALL
const balance = movements.reduce(function(accumulator,currentValue,i,arr){
  console.log(`Iteration ${i}: ${accumulator}`);//Iteration 0: 0, Iteration 1: 200, Iteration 2: 650.... ,Iteration 7: 2540
  return accumulator + currentValue;
},0); //0 is an initial value //if you put 5 => Iteration 0: 5, Iteration 1: 205, Iteration 2: 655 //balance is 3845

console.log(balance);//3840
```

```js
//Maximum value
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
const max = movements.reduce((acc,mov)=> {
  if(acc>mov)
    return acc;
  else
    return mov;
},movements[0]);
console.log(max);//3000
```

```js
//Example
const ages1 = [5,2,4,1,15,8,3];

const calcAverageHumanAge = function(ages){

    //1. Calculating the dog age in human years
    //if the dog is <=2, humanAge = 2*dogAge
    //if the dog is >2, humanAge = 16+dogAge*4
    //Solution: using MAP
    const humanAges = ages.map(age => age <=2 ? 2*age : 16+age*4);
    console.log(humanAges);//[36, 4, 32, 2, 76, 48, 28]

    //2. Exclude all dogs that are less than 18 human years old
    //Solution: using FILTER
    const adults = humanAges.filter(function(age){
        return age > 18;
    })
    console.log(adults);//[36, 32, 76, 48, 28]

    //3. calculate the average human age of all adult dogs
    //Solution: using REDUCE
    const average = adults.reduce((acc,curr) => {
        return acc+curr;
    },0) / adults.length;

    return average;
}

const avg1 = calcAverageHumanAge(ages1);
console.log(avg1);//44
```

```js
//Example of Chaining Methods
const ages1 = [5,2,4,1,15,8,3];

const calcAverageHumanAge = ages => 
    ages.map(age => age<=2 ? 2*age : 16+age*4)
        .filter(age => age>18)
        .reduce((acc,age,i,arr) => acc+age/arr.length,0);

const avg1 = calcAverageHumanAge(ages1);
console.log(avg1);//44
```

### The find Method
- Similar to Filter method, but there are two fundamental differences.
  - 1. The Filter method returns all the elements that match the condition while the Find method only returns the first one
  - 2. The Filter method returns a new array while Find only returns the element itself

  ```js
  const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
  const firstWithdrawal = movements.find(mov => mov < 0);
  console.log(firstWithdrawal);//-400
  ```

### The Findindex Method
- findindex returns the index of the found element not the element itself

### Some and Every
```js
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

//includes() only checks EQUALITY
console.log(movements.includes(-130));//true

//some() checks CONDITION
console.log(movements.some(mov => mov === -130));//true //this is used the same as includes()

//check all elements if there is any value more than 3000, if it does not exist, return false
const anyDeposits = movements.some(mov => mov > 3000);
console.log(anyDeposits);//false

//every(): if all element satisfy the condition, return true
console.log(movements.every(mov => mov > 0));//false

// Separate callback
const deposit = mov => mov > 0 ;
console.log(movements.some(deposit));//true
console.log(movements.every(deposit));//false
console.log(movements.filter(deposit));//[200, 450, 3000, 70, 1300]
```

### flat and flatMap
- flat()
- flatMap(): flat() + map(): it only goes to one level deep
  ```js
  const arr = [[1,2,3],[4,5,6],7,8];
  console.log(arr.flat());//[1,2,3,4,5,6,7,8]

  const arrDeep = [[[1,[2,3]],4],[5,[6,7]],8,9];
  console.log(arrDeep.flat());//[Array(2), 4, 5, Array(2), 8, 9]
  console.log(arrDeep.flat(1));//[Array(2), 4, 5, Array(2), 8, 9]
  console.log(arrDeep.flat(2));//[1, Array(2), 4, 5, 6, 7, 8, 9]
  console.log(arrDeep.flat(3));//1, 2, 3, 4, 5, 6, 7, 8, 9]
  ```

### Sorting Arrays
```js
//String
const owners = ['Jonas', 'Zach', 'Adam', 'Marta'];
console.log(owners.sort()); // ['Adam', 'Jonas', 'Marta', 'Zach']
//sort method mutate the original array owners
console.log(owners); // ['Adam', 'Jonas', 'Marta', 'Zach']

//Number
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
//it does not sort correctly because it's not a string
console.log(movements.sort());//[-130, -400, -650, 1300, 200, 3000, 450, 70]

//Sorting ascending order
//return <0, A, B (keep order)
//return >0, B, A (switch order)
movements.sort((a,b) => {
    if(a>b) return 1;
    if(a<b) return -1;
});

//Simply the method
movements.sort((a,b) => a-b);
console.log(movements);// [-650, -400, -130, 70, 200, 450, 1300, 3000]

//sorting descending order
movements.sort((a,b) => {
    if(a>b) return -1;
    if(a<b) return 1;
});

//Simply the method
movements.sort((a,b) => b-a);
console.log(movements);// [3000, 1300, 450, 200, 70, -130, -400, -650]
```

### More wys of Creating and Filling Arrays
- fill()
- Array.from()
  ```js
  console.log(new Array(1,2,3,4,5,6,7));//[1, 2, 3, 4, 5, 5, 6]

  //Array constructor function
  const x = new Array(7);
  console.log(x);//[empty × 7]
  //whenever we pass only one argument, it creates a new empty argument with that length

  //fill() method: mutate original array
  // x.fill(1);
  // console.log(x);//[1,1,1,1,1,1,1]
  // x.fill(1,3)
  // console.log(x);//[empty × 3, 1, 1, 1, 1]
  x.fill(1,3,5);//put 1 to index of 3 - 4
  console.log(x);//[empty × 3, 1, 1, empty × 2]

  //you can fill with the other array
  const arr = [1,2,3,4,5,5,6];
  arr.fill(23,4,6); //put 23 to index of 4 - 5
  console.log(arr);//[1, 2, 3, 4, 23, 23, 6]

  //Array.from() function
  const y = Array.from({length: 7}, () => 1);
  console.log(y);//[1, 1, 1, 1, 1, 1, 1]

  const z = Array.from({length:7}, (currentElement, index) => index+1);
  console.log(z);//[1, 2, 3, 4, 5, 6, 7]

  ```

### Which array method to use?
- To mutate original array
  - Add to original
    - .push (end)
    - .unshift (start)
  - Remove from original
    - .pop (end)
    - .shift (start)
    - .splice (any)
  - Others
    - .reverse
    - .sort
    - .fill

- A new array
  - Computed from original
    - .map (loop)
  - Filtered using condition
    - .filter
  - Portion of original
    - .slice
  - Adding original to other
    - .concat
  - Flattening the original
    - .flat
    - .flatMap

- An array index
  - Based on value
    - .indexOf
  - Based on test condition
    - .findIndex

- An array element
  - Based on test condition
    - .find

- Know if array includes
  - Based on value
    - .includes
  - Based on test condition
    - .some
    - .every

- A new string
  - Based on separator string
    - .join

- To transform to value
  - Based on accumulator
    - .reduce (Boil down array to single value of any type: number, string, boolean or even new array or object)

- To just loop array
  - Based on callback
    - .forEach (Does not create a new array, just loops over it)


### Practice
```js
const convertTitleCase = function(title){
    const exceptions = ['a','an', 'and', 'the','but','or','on','in','with'];
   
    //the first string has to be capitalized ex) and Here Is => And Here Is
    const capitalize = str => str[0].toUpperCase() + str.slice(1);
    
    //1. make everything lower case
    const titleCase1 = title.toLowerCase();
    console.log(titleCase1);//and here is another title with an example

    //2. split the setence with space and make an array
    const titleCase2 = titleCase1.split(' ');
    console.log(titleCase2);//['and', 'here', 'is', 'another', 'title', 'with', 'an', 'example']

    //3. if it includes exceptions, just leave it as a lower case, if not, only the first char change to Uppercase, and combine with the rest of the chars
    const titleCase3 = titleCase2.map(word => exceptions.includes(word) ? word : word[0].toUpperCase() + word.slice(1));
    console.log(titleCase3);//['and', 'Here', 'Is', 'Another', 'Title', 'with', 'an', 'Example']

    //4. put it together as a sentence
    const titleCase4 = titleCase3.join(' ');
    console.log(titleCase4);//and Here Is Another Title with an Example

    return capitalize(titleCase4);
}
console.log(convertTitleCase('and here is another title with an EXAMPLE'));//And Here Is Another Title with an Example
```

```js
//change to oneline code
const convertTitleCase = function(title){
    const exceptions = ['a','an', 'and', 'the','but','or','on','in','with'];
    const capitalize = str => str[0].toUpperCase() + str.slice(1);
    const titleCase = title.toLowerCase().split(' ').map(word => exceptions.includes(word) ? word : capitalize(word)).join(` `);
    return capitalize(titleCase);

}
console.log(convertTitleCase('this is a nice title'));//This Is a Nice Title
console.log(convertTitleCase('this is a LONG title but not too long'));//This Is a Long Title but Not Too Long
console.log(convertTitleCase('and here is another title with an EXAMPLE'));//And Here Is Another Title with an Example

```

```js

/* 
Julia and Kate are studying dogs, and they are studying if dogs are eating too much or too little.
Eating too much means the dog's current food portion is larger than the recommended portion, and eating too little is the opposite.
Eating an okay amount means the dog's current food portion is within a range 10% above and 10% below the recommended portion (see hint).

HINT 1: Use many different tools to solve these challenges, you can use the summary lecture to choose between them 😉
HINT 2: Being within a range 10% above and below the recommended portion means: current > (recommended * 0.90) && current < (recommended * 1.10). Basically, the current portion should be between 90% and 110% of the recommended portion.
*/

const dogs = [
  { weight: 22, curFood: 250, owners: ['Alice', 'Bob'] },
  { weight: 8, curFood: 200, owners: ['Matilda'] },
  { weight: 13, curFood: 275, owners: ['Sarah', 'John'] },
  { weight: 32, curFood: 340, owners: ['Michael'] }
]

//1. Loop over the array containing dog objects, and for each dog, calculate the recommended food portion and add it to the object as a new property. 
//Do NOT create a new array, simply loop over the array. 
//Forumla: recommendedFood = weight ** 0.75 * 28. 
dogs.forEach(dog => {
  dog.recFood = Math.trunc(dog.weight ** 0.75 * 28);
})

//2. Find Sarah's dog and log to the console whether it's eating too much or too little. 
//HINT: Some dogs have multiple owners, so you first need to find Sarah in the owners array, and so this one is a bit tricky (on purpose)
const dogSarah = dogs.find(dog => dog.owners.includes('Sarah'));
console.log(`Sarah's dog is eating ${dogSarah.curFood > dogSarah.recFood ? `much` : `little`}`);//Sarah's dog is eating much

//3. Create an array containing all owners of dogs who eat too much ('ownersEatTooMuch') and 
//an array with all owners of dogs who eat too little ('ownersEatTooLittle').
const ownersEatTooMuch = dogs.filter(dog => dog.curFood > dog.recFood).map(dog => dog.owners).flat();
console.log(ownersEatTooMuch);//['Matilda', 'Sarah', 'John']

//Using flatMap() instead
const ownersEatTooLittle = dogs.filter(dog => dog.curFood < dog.recFood).flatMap(dog => dog.owners);
console.log(ownersEatTooLittle);//['Alice', 'Bob', 'Michael']

//4. Log a string to the console for each array created in 3., like this: 
//"Matilda and Alice and Bob's dogs eat too much!" and "Sarah and John and Michael's dogs eat too little!"
console.log(`${ownersEatTooMuch.join(' and ')}'s dogs eat too much`);
console.log(`${ownersEatTooLittle.join(' and ')}'s dogs eat too little`);

//5. Log to the console whether there is any dog eating EXACTLY the amount of food that is recommended (just true or false)
console.log(dogs.some(dog => dog.curFood === dog.recFood));//false

//6. Log to the console whether there is any dog eating an OKAY amount of food (just true or false)
const checkEatingOkay = dog => dog.curFood  > (dog.recFood * 0.90) && dog.curFood < (dog.recFood * 1.10)
console.log(dogs.some(checkEatingOkay));//true

//7. Create an array containing the dogs that are eating an OKAY amount of food (try to reuse the condition used in 6.)
console.log(dogs.filter(checkEatingOkay))// {weight: 32, curFood: 340, owners: Array(1), recFood: 376}

//8. Create a shallow copy of the dogs array and sort it by recommended food portion in an ascending order 
//(keep in mind that the portions are inside the array's objects)
const dogsCopy = dogs.slice().sort((a,b) => a.recFood - b.recFood);
console.log(dogsCopy);

```
