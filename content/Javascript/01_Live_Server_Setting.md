+++
title = "Live Server Setting"
weight = 1
draft = false
+++

### Installing Live server
- VS Code Extension: Live Server
  - click Go Live

### Installing Node.js and Setting Up a Dev Environment
- in Terminal, ```node -v``` to check if it's installed
- ```npm install live-server -g```
- ```live-server```
    ```
    PS C:\Users\kelly\Workspaces\github\complete-javascript-course> live-server
    Serving "C:\Users\kelly\Workspaces\github\complete-javascript-course" at http://127.0.0.1:8080
    Ready for changes
    GET /favicon.ico 404 3.028 ms - 150
    ```