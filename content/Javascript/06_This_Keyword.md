+++
title = "This Keyword"
weight = 6
draft = false
+++


- **this keyword/variables**: special variable that is created for every execution context(every function). Takes the value of the "owner" of the function in which the this keyword is used

- **this** is **NOT** static: It depends on how the function is called, and its value is only assigned when the function is actually called
  - Simple function call: this => undefined (in strict mode)
  - Arrow functions: this => this of surrounding function
  - Method: this => Object that is calling the method
  - Event listener: this = DOM element that the handler is attached to

  ```js
  console.log(this); //Window

  //1. Simple function call => undefined (in strict mode)
  const calcAge = function(birthYear){
      console.log(2023 - birthYear); //31
      console.log(this); //undefined
  }
  calcAge(1992);

  //2. arrow function this => this of surrounding function
  const calcAgeArrow = (birthYear) => {
      console.log(2023 - birthYear); //31
      console.log(this); // Window
  }
  calcAgeArrow(1992);

  //3. Method => Object that is calling the method
  const kelly = {
      year: 1992,
      calcAge: function(){
          console.log(this); // {year: 1992, calcAge: ƒ}
          console.log(2023 - this.year); //31
      }
  };
  kelly.calcAge();


  const matilda = {
      year: 1991
  };

  //Method borrowing: borrow calcAge method from kelly
  matilda.calcAge = kelly.calcAge;
  console.log(matilda);//{year: 1991, calcAge: ƒ}

  // even this keyword is written in kelly object, this keyword is using Matilda
  matilda.calcAge();//32

  const f = jonas.calcAge;
  f(); //undefined, Uncaught TypeError: Cannot read property year of undefined
  ```

- Example
  ```js
    // This is not a code block. It's object literal
    const kelly = {
      firstName: 'Kelly',
      year: 1992,
      calcAge: function(){
        console.log(this); // {firstName: 'Kelly', year: 1992, calcAge: ƒ, greet: ƒ}
        console.log(2023 - this.year); //31

        // Issue: Simple function call => undefined (in strict mode)
        // const isMillenial = function(){
        //     console.log(this);//undefined
        //     console.log(this.year >= 1981 && this.year <= 1996);//Uncaught TypeError: Cannot read properties of undefined (reading 'year')
        // }
        // isMillenial();

        //Solution1: defining this to new variable
        // const self = this;
        // const isMillenial = function(){
        //     console.log(self);//31
        //     console.log(self.year >= 1981 && self.year <= 1996);//{firstName: 'Kelly', year: 1992, calcAge: ƒ, greet: ƒ}
        // }
        // isMillenial();

        //Solution2: using Arrow Function(arrow: this of surrounding function)
        const isMillenial = () => {
            console.log(this);//{firstName: 'Kelly', year: 1992, calcAge: ƒ, greet: ƒ}
            console.log(this.year >= 1981 && this.year <= 1996);//true
        }
        isMillenial();
      },
      //Arrow Function(this of surrounding function)
      greet: () => console.log(`Hey ${this.firstName}`),
      
      //Method => Object that is calling the method
      greet2: function(){
        console.log(`Hey ${this.firstName}`);
      }
    };
    kelly.calcAge(); 

    kelly.greet(); // Hey undefined // because arrow fuction's this is surrounding function, so it does not include firstName
    console.log(this.firstName); // undefined

    kelly.greet2(); //Hey Kelly

    ```

    <br>

### Argument Keyword
- Only exist in **regular function** (not in arrow function)
  ```js
  //1. Regular Function => it works
  const addExpr = function(a,b){
      console.log(arguments);
      return a+b;
  }

  addExpr(2,5);//Arguments(2) [2, 5, callee: (...), Symbol(Symbol.iterator): ƒ]
  addExpr(2,5,8,12);//Arguments(4) [2, 5, 8, 12, callee: (...), Symbol(Symbol.iterator): ƒ]


  //2. Arrow Function => it does not work
  var addArrow = (a,b) => {
      console.log(arguments);
      return a+b;
  }

  addArrow(2,5);//Uncaught ReferenceError: arguments is not defined at addArrow
  ```

<br>

### Primitive VS Objects
- **Primities**(Primitive Types: Number, String, Boolean, Undefined, Null, Symbol, BigInt): stored in **Call Stack**
- **Objects**(**Reference** Types: Object Literal, Arrays, Functions..): stored in **Heap**
```js
//1. Primitivs value example
let age = 30;
let oldAge = age;
age = 31;

console.log(age); //31
console.log(oldAge); //30

//2. Reference value example (Object)
const me = {
  name: 'Kelly',
  age: 30, // This is saved in HEAP
};

const friend = me;
friend.age = 27; //This modify in Heap

//both Friend and Me are poiting to same Address in Call Stack which reference to the same memory address in Heap
console.log(`Friend`, friend);//{name: 'Kelly', age: 27}
console.log(`Me`, me);//{name: 'Kelly', age: 27}
```

```js
//1. Primitivs value example
let lastName = 'Lee';
let oldLastName = lastName;
lastName = 'Clevinger';

console.log(lastName); //Clevinger
console.log(oldLastName); //Lee

//2. Reference value example
const kelly = {
  firstName: 'Kelly',
  lastName: 'Lee',
  age: 31
};

const marriedKelly = kelly;

//marriedKelly is const, but we can change what's inside because it's saved in Heap
marriedKelly.lastName = 'Clevinger';
console.log(`Before marriage:`,kelly);//Before marraige: {firstName: 'Kelly', lastName: 'Clevinger', age: 31}
console.log(`After marriage:`,marriedKelly);//After marriage: {firstName: 'Kelly', lastName: 'Clevinger', age: 31}
  
//Copying objects
const kelly2 = {
  firstName: 'Kelly',
  lastName: 'Lee',
  age: 31,
  family:['Alice','Bob']
};

//merge two objects
const kellyCopy = Object.assign({},kelly2);
kellyCopy.lastName = 'Cameron';

console.log(`Before marriage:`,kelly2);//{firstName: 'Kelly', lastName: 'Lee', age: 31}
console.log(`After marriage:`,kellyCopy);//{firstName: 'Kelly', lastName: 'Clevinger', age: 31}

kellyCopy.family.push('Mary');
kellyCopy.family.push('Josh');

//still issue / object.assign did not work well here, it needs deep clone
console.log(`Before marriage:`,kelly2);// ['Alice', 'Bob', 'Mary', 'Josh']
console.log(`After marriage:`,kellyCopy);//['Alice', 'Bob', 'Mary', 'Josh']
```