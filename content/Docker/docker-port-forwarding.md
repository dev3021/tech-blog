+++
title = "Docker Port Forwarding"
weight = 2
draft = false
+++


### Port-forwarding
- By default, when you create or run a container using docker create or docker run, the container doesn’t expose any of it’s ports to the outside world. To make a port available to services outside of Docker, or to Docker containers running on a different network, use the --publish or -p flag.
- ``` -p [Host Port]:[Container Port]```

- **Dockerfile**
  - ```EXPOSE 3000```: This is just for documention which port you are going to expose
    


- Example 1:
  - ```-p 8080:80```: The traffic comes to Host 8080, and gets forwarded to docker container 80 port
  - ```http://localhost:8080```
  - ```0.0.0.0:8080->80/tcp```

- Example 2: 
  - ```EXPOSE 3000 ``` (Dockerfile)
  - ```-p 80:3000```: The traffic comes to host 80, and gets forwarded to docker container 3000 port
  - ```http://localhost:80```

- Example 3:
  - ```docker run -p 3000:3000 kellylee0475/reactapp``` works with ```localhost:3000```
  - ```docker run -p 80:3000 kellylee0475/reactapp``` works with ```localhost:80```
  - ```docker run -p 3000:80 kellylee0475/reactapp``` does not work with **localhost:3000 does not work!** => it's because reactapp is running on 3000 as a default in container, so you need to change react configuration first
    - ```"start": "PORT=80 react-scripts start"``` in package.json file
    - Dockerfile ```EXPOSE:80```
    - build a new image ```docker build -t kellylee0475/react80```
    - ```docker run -p 3000:80 kellylee0475/react80```
    - now you can go to ```localhost:3000```



### Checking port in use in ubuntu
- sudo netstat -tulpn
- sudo netstat -tulpn | grep LISTEN
