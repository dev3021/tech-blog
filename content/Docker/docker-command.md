+++
title = "Docker Command"
weight = 1
draft = false
+++


### Docker Commands
- ```docker info```

- ```docker images```

- ```docker ps```: process status

- ```docker ps -a```: -all or -a show all of your containers, current running or stopped

- ``` docker container prune```

- ```docker images prune``` => remove all untagged images

- ```docker system prune``` : cleans up dangling images, containers, volumes, and networks

- ```docker system prune -a```: additionally remove any stopped containers and all unused images

- ```docker run -it -d -p 80:3000 kellylee0475/reactapp```

- ```-it```
  - it's a combination of two flags, **-i** and **-t**, it allows you to run the container with an interactive pseudo-TTY so that you can issue commands.
- ```-d```
  - This flag allows us to run our container 'detached', meaning it will continue to run in the background. Imagine needing to keep your terminal attached to your server at all times to kepp it running! In fact, you can try this by running the same command without the **-d** flag, and find that as soon as you exit the container, it will be stopped
- ```-p```
  - This stands for publish, but allows you to map the host machine's port to an exposed port on the container. If you don't know what ports have been exposed, a capital **-P** will expose all exposable ports to random ports
- ```--name```
  - You can name your running container. 
